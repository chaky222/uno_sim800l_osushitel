#include <Wire.h>

#define SCL_PIN 2 
#define SCL_PORT PORTD 
#define SDA_PIN 3 
#define SDA_PORT PORTD 
#define I2C_CPUFREQ (F_CPU/16)
#define I2C_SLOWMODE 1 
#define BMAADDR 0x40
#include <SoftI2C.h>

int xval, yval, zval;

void setup() {

  // put your setup code here, to run once:
Serial.begin(115200);
 while (!Serial) 
    {
    }
  // i2c_init(); 
  if (!initBma()) {
    Serial.println(F("INIT ERROR"));
  }

  Serial.println ();
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;
  
  Wire.begin();
  for (byte i = 8; i < 120; i++)
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
      {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
      } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
}  // end of setup

void loop(void){
  if (!readBma()) Serial.println(F("READ ERROR"));
  Serial.print(F("X="));
  Serial.print(xval);
  Serial.print(F("  Y="));
  Serial.print(yval);
  Serial.print(F("  Z="));
  Serial.println(zval);
  delay(1000);
}






bool setControlBits(uint8_t cntr)
{
  Serial.println(F("Soft reset"));
  if (!i2c_start(BMAADDR | I2C_WRITE)) {
    return false;
  }
  if (!i2c_write(0x0A)) {
    return false;
  }
  if (!i2c_write(cntr)) {
    return false;
  }
  i2c_stop();
  return true;
}

bool initBma(void)
{
  if (!setControlBits(B00000010)) return false;;
  delay(100);
  return true;
}

int readOneVal(bool last)
{
  uint8_t msb, lsb;
  lsb = i2c_read(false);
  msb = i2c_read(last);
  return (int)((msb<<8)|lsb)/64;
}

bool readBma(void)
{
  xval = 0xFFFF;
  yval = 0xFFFF;
  zval = 0xFFFF;
  if (!i2c_start(BMAADDR | I2C_WRITE)) return false;
  if (!i2c_write(0x02)) return false;
  if (!i2c_rep_start(BMAADDR | I2C_READ)) return false;
  xval = readOneVal(false);
  yval = readOneVal(false);
  zval = readOneVal(true);
  i2c_stop();
  return true;
}




