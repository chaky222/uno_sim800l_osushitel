#include <SoftwareSerial.h>

SoftwareSerial gprsSerial(12, 8);
// SoftwareSerial gprsSerial(8, 12);


uint8_t accepted_pins[] = {13,7,11,10,9,6,5,4};


void setup()
{
    Serial.begin(115200);
    Serial.println("Start1");
    gprsSerial.begin(1200); 
    delay(500);
    Serial.println("Start2");
    gprsSerial.print("AT+CMGF=1\r");
    delay(500);
    Serial.println("Start3");
    gprsSerial.print("AT+IPR=1200\r");
    delay(500);
    // Serial.println("Start4");
    // gprsSerial.print("ATE1\r");
    // delay(3500);
    // gprsSerial.print("ATV1\r");
    // delay(3500);
    // gprsSerial.print("AT+CMEE=2\r");
    // delay(1500);
    Serial.println("Start5");
    gprsSerial.print("AT+IFC=1, 1\r");
    delay(1500);
    gprsSerial.print("AT+IFC=1, 1\r");
    delay(1500);
    gprsSerial.print("AT+CPBS=\"SM\"\r");
    delay(1500);
    gprsSerial.print("AT+CNMI=1,2,2,1,0\r");
    delay(1500);
    Serial.println("Start6");
    gprsSerial.print("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r");//AT+SAPBR=3,1,«CONTYPE»,«GPRS»
    delay(1500);
    gprsSerial.print("AT+SAPBR=3,1,\"APN\",\"www.kyivstar.net\"\r");//AT+SAPBR=3,1,«APN»,«internet.beeline.ru»
    delay(1500);
    gprsSerial.print("AT+SAPBR=3,1,\"USER\",\"igprs\"\r");//AT+SAPBR=3,1,«USER»,«beeline»
    delay(1500);
    gprsSerial.print("AT+SAPBR=3,1,\"PWD\",\"internet\"\r");//AT+SAPBR=3,1,«PWD»,«beeline»
    delay(1500);
    gprsSerial.print("AT+SAPBR=1,1\r");//AT+SAPBR=1,1 — установка GPRS связи
    delay(3500);
    gprsSerial.print("AT+SAPBR=2,1\r");//AT+SAPBR=2,1 — полученный IP адрес  +SAPBR: 1,1,«10.229.9.115»
    delay(2500);
    Serial.println("Start7");
    gprsSerial.print("AT+SAPBR=4,1\r");//AT+SAPBR=4,1 — текущие настройки соединения
    delay(2500);
    Serial.println("Start8");
}

uint8_t income_cmd[3] = {0,0,0};
char    cmdbuffer[10+2];
uint8_t buffer_pos = 0;
uint8_t current_interface = 0; // 0=serial, 1=GSM_data, 2=sms, 3=tone dtmf, 4=GPRS


void DigitalWritePin(){
  bool access = false;
  for (uint8_t u = 0; u< sizeof(accepted_pins); u++){
    if (accepted_pins[u] == income_cmd[1]){
      access = true;
      break;
    }
  }
  if (access){
    pinMode(income_cmd[1], OUTPUT);
    digitalWrite(income_cmd[1], income_cmd[2]);
  }else{
    Serial.print("NO ACCESS");
  }          
}


void try_get_and_run_cmd(){  
  Serial.print("cmdbuffer=[");
  Serial.print(cmdbuffer);
  Serial.println("]");
  for (uint8_t u =0;u<3;u++) income_cmd[u] = 0;
  for (uint8_t u =0;u<3;u++){
    if (buffer_pos < u*3+3) break;   
    char tmp[4];
    strcpy(&(tmp[0]),&cmdbuffer[u*3]);
    tmp[3] = 0;
    uint16_t tmp16 = strtol(&tmp[0], NULL, 10);     
    income_cmd[u] = (tmp16 < 256) ? tmp16 : 0;       
    Serial.print(" p");  
    Serial.print(u);  
    Serial.print("=");
    Serial.print(income_cmd[u]);     
  } 
  Serial.println(" ");
  switch(income_cmd[0]){
    case 0:
      current_interface = 1;
      break;
    case 1: //Digital write pin
      DigitalWritePin();
      break;
    default:
      if (income_cmd[0] > 0){
        Serial.print("Unknown cmd=");
        Serial.println(income_cmd[0]);
      }
      break;
  }
}

void loop()
{
  char currSymb=0;
  if (gprsSerial.available()){
    currSymb = gprsSerial.read();
    // if (!((current_interface >= 1) &&  (current_interface <=8)))  {  //0=serial, 1=GSM_data, 3=sms,4=sms_ready, 5=dtmf, 6=dtmf ready, 7=GPRS, 8=GPRS ready.
    //   Serial.println("");
    //   Serial.print("current_interface 1 !=");
    //   Serial.println(current_interface);
    //   buffer_pos = 0;
    //   current_interface = 1;
    // }
    Serial.print(currSymb);
    // Serial.println(currSymb);
    // if ((currSymb == '#')){
    //   Serial.println("sharp detected");
    // }
  }

  if (Serial.available() > 0) {
    char currSymb1 = Serial.read();    
    // if (current_interface != 0){// 0=serial
    //   Serial.println("");
    //   Serial.print("current_interface 0 !=");
    //   Serial.println(current_interface);
    //   // buffer_pos = 0;
    //   // current_interface = 0;
    // }
    Serial.print("serial_char=");
    Serial.println(currSymb1);
    if (currSymb1 == 'a'){
      gprsSerial.print("AT+SAPBR=4,1\r");//AT+SAPBR=4,1 — текущие настройки соединения
      // delay(2500);
    }
    if (currSymb1 == 'b'){
      gprsSerial.print("AT+SAPBR=2,1\r");//AT+SAPBR=2,1 — полученный IP адрес  +SAPBR: 1,1,«10.229.9.115»
    }
    // delay(2500);
    // if (currSymb1 > 0) currSymb = currSymb1;
  }

  // if (currSymb > 0){
  //   if ((buffer_pos >= 8) ||(currSymb == '#') || ( currSymb == '\r')){
  //     if (buffer_pos > 0){
  //       if ((currSymb == '#') || ( currSymb == '\r')){
  //         cmdbuffer[buffer_pos] = 0;
  //       }else{
  //         cmdbuffer[buffer_pos] = currSymb;
  //         cmdbuffer[buffer_pos+1] = 0;
  //       }
  //       buffer_pos++;
  //       if(strstr(cmdbuffer,"+CMT: ")!=NULL) {
  //         current_interface = 3;
  //       }
  //       if ((current_interface%2) == 0)  { // IF ready for command //0=serial, 1=GSM_data, 3=sms,4=sms_ready, 5=dtmf, 6=dtmf ready, 7=GPRS, 8=GPRS ready.
  //           // Serial.println("try_get_and_run_cmd_55");
  //           try_get_and_run_cmd();            
  //       }else{
          
  //         if (( currSymb == '\r') && (current_interface>=3) && (current_interface<8) && ((current_interface % 2) == 1) ) {
  //           current_interface++;
  //           // Serial.println("");
  //           // Serial.println("SMS START line!");
  //         }
  //       }
  //     }
  //     buffer_pos = 0;
  //   }else if (('\n' != currSymb) && (buffer_pos < 10)) {
  //     cmdbuffer[buffer_pos] = currSymb;
  //     buffer_pos++;    
  //   }
  // }
  


}