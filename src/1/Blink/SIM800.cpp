/*************************************************************************
* SIM800 GPRS/HTTP Library
* Distributed under GPL v2.0
* Written by Stanley Huang <stanleyhuangyc@gmail.com>
* For more information, please visit http://arduinodev.com
*************************************************************************/

#include "SIM800.h"


bool CGPRS_SIM800::setBoudRate(){
  uint16_t baudRates[] = {1200, 2400, 4800, 9600, 19200, 38400, 57600};
  SIM_SERIAL2.begin(9600);
  if (sendCommandPGM(PSTR("AT+IPR=1200"), 1000) > 0) {
    delay(300);
    SIM_SERIAL2.begin(1200);
    delay(1000);
    return true;
  }
  for (uint8_t i=0;i<7;i++){
    SIM_SERIAL2.begin(baudRates[i]);
    delay(500);
    if (sendCommandPGM(PSTR("AT+IPR=1200"), 4000) > 0) {
      SIM_SERIAL2.begin(1200);
      purgeSerial();
      delay(1000);
      return true;
    }
  }
  return false;
}


bool CGPRS_SIM800::init()
{    
    #ifdef DEBUG
      SERIAL_ECHOLNPGM("init");    
    #endif
    pinMode(SIM800_RESET_PIN, OUTPUT);
    digitalWrite(SIM800_RESET_PIN, HIGH);
    delay(10);
    digitalWrite(SIM800_RESET_PIN, LOW);
    delay(100);
    digitalWrite(SIM800_RESET_PIN, HIGH);
    delay(3000);
    #ifdef DEBUG
      SERIAL_ECHOLNPGM("setBoudRate");    
    #endif
    setBoudRate();   
     #ifdef DEBUG
      SERIAL_ECHOLNPGM("run init");    
    #endif 
    if (sendCommandPGM(PSTR("AT"))) {
        sendCommandPGM(PSTR("AT+IPR=1200"));
        sendCommandPGM(PSTR("ATE0"));
        #ifdef DEBUG
          SERIAL_ECHOLNPGM("run CFUN=1");    
        #endif 
        sendCommandPGM(PSTR("AT+CFUN=1"), 10000);
        purgeSerial();
        delay(1000);
        purgeSerial();
        return true;
    }
    // if (sendCommandPGM(PSTR("AT", 10000) > 0) {
    //     sendCommandPGM(PSTR("AT+IPR=1200");
    //     sendCommandPGM(PSTR("ATE0", 10000); 
    //     // #ifdef DEBUG
    //     //   SERIAL_ECHOLNPGM("run ATV0");    
    //     // #endif 
    //     // sendCommandPGM(PSTR("AT+IPR=1200");
    //     // sendCommandPGM(PSTR("ATV0", 10000);
    //     #ifdef DEBUG
    //       SERIAL_ECHOLNPGM("run CFUN=1");    
    //     #endif 
    //     sendCommandPGM(PSTR("AT+CFUN=1", 10000);
    //     // sendCommandPGM(PSTR("AT+CMGF=1", 10000);    // sets the SMS mode to text
    //     // sendCommandPGM(PSTR("AT+CPMS=\"SM\",\"SM\",\"SM\"", 10000); // selects the memory
    //     return true;
    // }
    return false;
}
uint8_t CGPRS_SIM800::setup(){
  bool success = false;
  #ifdef DEBUG
    SERIAL_ECHOLNPGM("run setup");
  #endif
  for (uint8_t n = 0; n < 30; n++) {
    // SERIAL_ECHOLNPGM("run CREG");
    if (sendCommandPGM(PSTR("AT+CREG?"), 10000)>0) {
        // SERIAL_ECHOLNPGM("CREG good");
        char *p = strstr(buffer, "0,");
        if (p) {
          char mode = *(p + 2);
#ifdef DEBUG
          SERIAL_PROTOCOLPGM("Mode:");
          Serial.println(mode);
#endif
          if (mode == '1' || mode == '5') {
            success = true;
            break;
          }
        }
    }else{
      SERIAL_ECHOLNPGM("Buffer=");
      Serial.println(buffer);
    }
    delay(2000);
  }
  		
  if (!success)                                                                return 1;  
  if (!(sendCommandPGM(PSTR("AT+CGATT?"), 5000) > 0))                          return 2;    
  if (!(sendCommandPGM(PSTR("AT+SAPBR=3,1,\"Contype\",\"GPRS\""), 10000) > 0)) return 3;
  
  // GSM_PROTOCOLPGM();
  // GSM_PROTOCOLPGM("www.kyivstar.net");
  // GSM_PROTOCOLPGM("www.ab.kyivstar.net");
  // SIM_SERIAL2.print("\"");
  // if (!sendCommandPGM(PSTR("AT+SAPBR=3,1,\"APN\",\"www.kyivstar.net\""), 10000))    return 4;
  if (!sendCommandPGM(PSTR("AT+SAPBR=3,1,\"APN\",\"www.ab.kyivstar.net\"" ), 10000)) return 4;
  if (!sendCommandPGM(PSTR("AT+SAPBR=3,1,\"USER\",\"igprs\""              ), 10000)) return 7;
  if (!sendCommandPGM(PSTR("AT+SAPBR=3,1,\"PWD\",\"internet\""            ), 10000)) return 8;

  // GSM_PROTOCOLPGM("AT+SAPBR=3,1,\"USER\",\"");
  // GSM_PROTOCOLPGM("igprs");
  // SIM_SERIAL2.print('\"');
  // if (!sendCommand(0))
  //   return 4;
  // GSM_PROTOCOLPGM("AT+SAPBR=3,1,\"PWD\",\"");
  // GSM_PROTOCOLPGM("internet");
  // SIM_SERIAL2.print('\"');
  // if (!sendCommand(0))
  //   return 4;
  
  sendCommandPGM(PSTR("AT+SAPBR=1,1"), 10000);
  sendCommandPGM(PSTR("AT+SAPBR=2,1"), 10000);

 

  if (!success)
    return 5;

  return 0;
}
bool CGPRS_SIM800::getOperatorName()
{
  // display operator name
  if (sendCommand("AT+COPS?", "OK\r", "ERROR\r") == 1) {
      char *p = strstr(buffer, ",\"");
      if (p) {
          p += 2;
          char *s = strchr(p, '\"');
          if (s) *s = 0;
          strcpy(buffer, p);
          return true;
      }
  }
  return false;
}
bool CGPRS_SIM800::checkSMS()
{
  SERIAL_ECHOLNPGM("checkSMS");
  if (sendCommand("AT+CMGR=1", "+CMGR:", "ERROR") == 1) {
    // reads the data of the SMS
    sendCommand(0, 100, "\r\n");
    if (sendCommand(0)) {
      // remove the SMS
      sendCommandPGM(PSTR("AT+CMGD=1"));
      return true;
    }
  }
  return false; 
}
int CGPRS_SIM800::getSignalQuality()
{
  sendCommandPGM(PSTR("AT+CSQ"));
  char *p = strstr(buffer, "CSQ: ");
  if (p) {
    int n = atoi(p + 2);
    if (n == 99 || n == -1) return 0;
    return n * 2 - 114;
  } else {
   return 0; 
  }
}

void CGPRS_SIM800::httpUninit()
{
  sendCommandPGM(PSTR("AT+HTTPTERM"));
}

bool CGPRS_SIM800::httpInit()
{
  if  (!sendCommandPGM(PSTR("AT+HTTPINIT"), 10000) || !sendCommandPGM(PSTR("AT+HTTPPARA=\"CID\",1"), 5000)) {
    httpState = HTTP_DISABLED;
    return false;
  }
  httpState = HTTP_READY;
  return true;
}
bool CGPRS_SIM800::httpConnect(const char in_url[] PROGMEM, const char* args)
{
    // Sets url
    GSM_PROTOCOLPGM("AT+HTTPPARA=\"URL\",\"");
    gsmPrintPGM(in_url);
    #ifdef DEBUG
      SERIAL_PROTOCOLLNPGM("ASK CMD=");
      SERIAL_PROTOCOLPGM("AT+HTTPPARA=\"URL\",\"");
      serialprintPGM(in_url);
    #endif 
    // Serial.print(url);
    if (args) {        
        // GSM_PROTOCOLPGM("?");
        SIM_SERIAL2.print(args);
        #ifdef DEBUG
          // SERIAL_PROTOCOLPGM("?");
          Serial.print(args);
        #endif 
    }

    SIM_SERIAL2.println('"');
    #ifdef DEBUG
      Serial.println('"');
    #endif 
    if (sendCommand(0))
    {
      #ifdef DEBUG  
        SERIAL_PROTOCOLLNPGM("GOOD, try AT+HTTPACTION=0");
      #endif
        // Starts GET action
        GSM_PROTOCOLLNPGM("AT+HTTPACTION=0");
        httpState = HTTP_CONNECTING;
        m_bytesRecv = 0;
        m_checkTimer = millis();
    } else {
        httpState = HTTP_ERROR;
    }
    return false;
}
// check if HTTP connection is established
// return 0 for in progress, 1 for success, 2 for error
uint8_t CGPRS_SIM800::httpIsConnected()
{
    uint8_t ret = checkbuffer("0,200", "0,60", 10000);
    if (ret >= 2) {
        httpState = HTTP_ERROR;
        return -1;
    }
    return ret;
}
void CGPRS_SIM800::httpRead()
{
    GSM_PROTOCOLLNPGM("AT+HTTPREAD");
    httpState = HTTP_READING;
    m_bytesRecv = 0;
    m_checkTimer = millis();
}
// check if HTTP connection is established
// return 0 for in progress, -1 for error, number of http payload bytes on success
int CGPRS_SIM800::httpIsRead()
{
    uint8_t ret = checkbuffer("+HTTPREAD: ", "Error", 10000) == 1;
    if (ret == 1) {
        m_bytesRecv = 0;
        // read the rest data
        sendCommand(0, 100, "\r\n");
        int bytes = atoi(buffer);
        sendCommand(0);
        bytes = min(bytes, sizeof(buffer) - 1);
        buffer[bytes] = 0;
        return bytes;
    } else if (ret >= 2) {
        httpState = HTTP_ERROR;
        return -1;
    }
    return 0;
}

uint8_t CGPRS_SIM800::sendCommandPGM(const char cmd[] PROGMEM, unsigned int timeout, const char* expected){
   // purgeSerial();
  char ch=pgm_read_byte(cmd);
  if (ch > 0) {
    purgeSerial();
  #ifdef DEBUG
      SERIAL_PROTOCOLPGM("cmdPGM>");
      serialprintPGM(cmd);
      Serial.println();
  #endif
      gsmPrintPGM(cmd);
      SIM_SERIAL2.println();
    }else{
      #ifdef DEBUG
        SERIAL_PROTOCOLPGM("cmdPGM2>");
        Serial.println('0');
     #endif
    }
    uint32_t t = millis();
    uint8_t n = 0;
    do {
      if (SIM_SERIAL2.available()) {
        char c = SIM_SERIAL2.read();
        #ifdef DEBUG
          // SERIAL_PROTOCOLPGM("c=");
          // SERIAL_ECHOLNPGM(c);
        #endif
        if (n >= sizeof(buffer) - 1) {
          // buffer full, discard first half
          n = sizeof(buffer) / 2 - 1;
          memcpy(buffer, buffer + sizeof(buffer) / 2, n);
        }
        buffer[n++] = c;
        buffer[n] = 0;
        if (strstr(buffer, expected ? expected : "OK\r")) {
  #ifdef DEBUG
         // SERIAL_PROTOCOLPGM("[1]");
         // Serial.println(buffer);
  #endif
         return n;
        }
      }
    } while (millis() - t < timeout);
  #ifdef DEBUG
     // SERIAL_PROTOCOLPGM("[0]");
     // Serial.println(buffer);
  #endif
    return 0;
}

uint8_t CGPRS_SIM800::sendCommand(const char* cmd, unsigned int timeout, const char* expected)
{
  // purgeSerial();
  if (cmd) {
    purgeSerial();
#ifdef DEBUG
    // SERIAL_PROTOCOLPGM("cmd1>");
    // serialprintPGM(PSTR(cmd));
    // Serial.println(cmd);
#endif
    // gsmPrintPGM(PSTR(cmd));
    SIM_SERIAL2.println(cmd);
  }else{
    // #ifdef DEBUG
      // SERIAL_PROTOCOLPGM("cmd2>");
      // Serial.println('0');
   // #endif
  }
  uint32_t t = millis();
  uint8_t n = 0;
  do {
    if (SIM_SERIAL2.available()) {
      char c = SIM_SERIAL2.read();
      #ifdef DEBUG3
        // SERIAL_PROTOCOLPGM("c=");
        // SERIAL_ECHOLNPGM(c);
      #endif
      if (n >= sizeof(buffer) - 1) {
        // buffer full, discard first half
        n = sizeof(buffer) / 2 - 1;
        memcpy(buffer, buffer + sizeof(buffer) / 2, n);
      }
      buffer[n++] = c;
      buffer[n] = 0;
      if (strstr(buffer, expected ? expected : "OK\r")) {
#ifdef DEBUG
       SERIAL_PROTOCOLPGM("[1]");
       Serial.println(buffer);
#endif
       return n;
      }
    }
  } while (millis() - t < timeout);
#ifdef DEBUG
   SERIAL_PROTOCOLPGM("[0]");
   Serial.println(buffer);
#endif
  return 0;
}
uint8_t CGPRS_SIM800::sendCommand(const char* cmd, const char* expected1, const char* expected2, unsigned int timeout)
{
  if (cmd) {
    purgeSerial();
#ifdef DEBUG
    SERIAL_PROTOCOLPGM("c8=>");
    // serialprintPGM(PSTR(cmd));
    Serial.println(cmd);
#endif
    // gsmPrintPGM(PSTR(cmd));
    SIM_SERIAL2.println(cmd);
  }
  uint32_t t = millis();
  uint8_t n = 0;
  do {
    if (SIM_SERIAL2.available()) {
      char c = SIM_SERIAL2.read();
      #ifdef DEBUG
        // SERIAL_PROTOCOLPGM("c2=");
        // Serial.println(c);
      #endif
      if (n >= sizeof(buffer) - 1) {
        // buffer full, discard first half
        n = sizeof(buffer) / 2 - 1;
        memcpy(buffer, buffer + sizeof(buffer) / 2, n);
      }
      buffer[n++] = c;
      buffer[n] = 0;
      if (strstr(buffer, expected1)) {
#ifdef DEBUG
       SERIAL_PROTOCOLPGM("[1]");
       Serial.println(buffer);
#endif
       return 1;
      }
      if (strstr(buffer, expected2)) {
#ifdef DEBUG
       SERIAL_PROTOCOLPGM("[2]");
       Serial.println(buffer);
#endif
       return 2;
      }
    }
  } while (millis() - t < timeout);
#ifdef DEBUG
   SERIAL_PROTOCOLPGM("[0]");
   Serial.println(buffer);
#endif
  return 0;
}

uint8_t CGPRS_SIM800::checkbuffer(const char* expected1, const char* expected2, unsigned int timeout)
{
    // m_bytesRecv = 0;
    while (SIM_SERIAL2.available()) {
        char c = SIM_SERIAL2.read();
        #ifdef DEBUG2
          SERIAL_PROTOCOLPGM("c3=");
          Serial.println(c);
        #endif
        if (m_bytesRecv >= sizeof(buffer) - 1) {
            // buffer full, discard first half
            m_bytesRecv = sizeof(buffer) / 2 - 1;
            memcpy(buffer, buffer + sizeof(buffer) / 2, m_bytesRecv);
        }
        buffer[m_bytesRecv++] = c;
        buffer[m_bytesRecv] = 0;
        if (strstr(buffer, expected1)) {
            return 1;
        }
        if (expected2 && strstr(buffer, expected2)) {
            return 2;
        }
    }
    return (millis() - m_checkTimer < timeout) ? 0 : 3;
}

void CGPRS_SIM800::purgeSerial()
{
  while (SIM_SERIAL2.available()){
    char c = SIM_SERIAL2.read();
    #ifdef DEBUG2
      SERIAL_PROTOCOLPGM("c_purge=");
      Serial.println(c);
    #endif
  }
}

