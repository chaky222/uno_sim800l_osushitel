
#include "plan.h"
#include <avr/interrupt.h>
#include <avr/io.h>



// #define cmd_success     1
// #define cmd_lock_thread 2

void plan_class::send_debug_info(){ 
  // Serial.print("debug");
  // Serial.println(secs);
  // Serial.print(F("s1 runned_init="));
  // Serial.print(s1->runned_init);
  // Serial.print(F("broken="));
  // Serial.print(s1->broken);
  // Serial.print(F(" s2_runned_init="));
  // Serial.print(s2->runned_init);
  // Serial.print(F("broken="));
  // Serial.println(s2->broken);

  // return;
  // if (s1->_si_exists){
  #ifdef DEBUG_SENSOORS
    si7021_thc data = s1->getTempAndRH();
    SERIAL_ECHOPGM("data celsiusHundredths=");
    Serial.print(data.celsiusHundredths); 
    SERIAL_ECHOPGM(" humidityPercent=");
    Serial.println(data.humidityPercent);
    Serial.println();
  // }else Serial.println("Sensor 1 is absent"); 
  // if (s2->_si_exists){
    // Serial.print("d444");
    si7021_thc data2 = s2->getTempAndRH();
    SERIAL_ECHOPGM("data2 celsiusHundredths=");
    Serial.print(data2.celsiusHundredths); 
    SERIAL_ECHOPGM(" humidityPercent=");
    Serial.println(data2.humidityPercent);
    Serial.println();
  #endif
 // }else Serial.println("Sensor 2 is absent"); 
  return;
  uint16_t c2 = tim2,c1 = calc1, s1 = secs;   tim2=0;    calc1=0; 
  // Serial.println();
  // clog("tim2=", c2,false);   
  // clog(" calc1=", c1,false);  
  // clog(" secs=", s1,false);  
  // clog(" last_time=", s.last_unix_timestamp,false);    
  // Serial.println("");
  Serial.flush();
  // process_command(16777216);
}

plan_class::plan_class(){
  for (uint8_t i = 0; i<HISTORY_BUFFER_SIZE; i++){s.history_timestamp[i] = 0;}
  for (uint8_t i = 0; i<DATA_FROM_SENSOR_SIZE; i++){
    s.sensor1_timestamp[i] = 0;
    s.sensor2_timestamp[i] = 0;
  }
  for (uint8_t i = 0; i<MAX_INCOME_CMD_LENGTH; i++){
    income_serial[i] = 0;
    income_gsm[i] = 0;
  }
  // s2->soft_i2c = true;
  // gprsSerial.begin(1200); 
}


volatile void plan_class::init(){
  uint8_t a[2]={2,2};
  // uint8_t a[4]={5,5,5,5};
  uint16_t cmd = *((uint16_t*) a);
  pushIncomeCMD(cmd);  
  // si7021_thc data2 = s1->getTempAndRH();
             // data2 = s2->getTempAndRH();
  // s1->begin();
  // delay(10);  
  // s2->begin();
}

void plan_class::main_thread_tick(){
  // В этой процедуре надо делать всего по минимуму.... только читаем со входных портов и слаживаем в массив. Обрабатываться эта каша будет уже во второстепенных потоках.
  fast_ticks++;
  if (fast_ticks >= 2048){
    fast_ticks = 0;
    secs++;
    without_send_data_seconds++;
    if (without_send_data_seconds == 255) while(1){}; // reboot now!   
    if (power_off_secondary_seconds > 0){
      power_off_secondary_seconds--;
      if (power_off_secondary_seconds == 0) digitalWrite(POWER_SECONDARY_PIN, LOW);   
    }
    if ((!(s1->broken)) && (s1->runned_init)&& (s1->try_init_timeout > 0)){
      wdt_reset();
      // Serial.println("s1 runned_init");
      s1->try_init_timeout--;
      if (s1->try_init_timeout == 0){
        s1->broken = true;
        digitalWrite(s1->pwr_pin,LOW); 
        #ifdef DEBUG_SENSOORS
          Serial.println(F("ERROR in begin s1"));
        #endif
        process_lock_status = cmd_success;
        timer_div_is_buzy[2] = false;
        uint8_t a[2]={5,5};
        uint16_t cmd = *((uint16_t*) a);
        pushIncomeCMD(cmd); 
      }
    }
    if ((!(s2->broken)) && (s2->runned_init) && (s2->try_init_timeout > 0)){
      wdt_reset();
      // Serial.println("s2 runned_init");
      s2->try_init_timeout--;
      if (s2->try_init_timeout == 0){
        s2->broken = true;
        digitalWrite(s2->pwr_pin,LOW); 
        #ifdef DEBUG_SENSOORS
          Serial.println(F("ERROR in begin s2"));
        #endif
        process_lock_status = cmd_success;
        timer_div_is_buzy[2] = false;
        uint8_t a[2]={5,5};
        uint16_t cmd = *((uint16_t*) a);
        pushIncomeCMD(cmd); 
      }
    }
    if (s1->broken){
      s1->broken_seconds++;
      if (s1->broken_seconds == 20){        
        s1->broken_seconds = 0;
        #ifdef DEBUG_SENSOORS
          Serial.println(F("try repair s1"));
        #endif
        s1->broken = false;
        s1->runned_init = false;
      }else{
        #ifdef DEBUG_SENSOORS
          Serial.print(F("s1 broken_seconds="));
          Serial.println(s1->broken_seconds);
        #endif
      }
    }
    if (s2->broken){
      s2->broken_seconds++;
      if (s2->broken_seconds == 20){        
        s2->broken_seconds = 0;
        #ifdef DEBUG_SENSOORS
          Serial.println(F("try repair s2"));
        #endif
        s2->broken = false;
        s2->runned_init = false;
      }else{
        #ifdef DEBUG_SENSOORS
          Serial.print(F("s2 broken_seconds="));
          Serial.println(s2->broken_seconds);
        #endif
      }
    }    
  }

  if ((!need_search_cmd_in_serial) && (Serial.available())){
    char inChar = (char)Serial.read();
    if (((inChar == '\n') || (inChar == '\r')) && (!(income_serial[0] == 0))) {
      // need_search_cmd_in_serial = true;
      pushIncomeCMD(strtol(&income_serial[0], NULL, 10));
      income_serial[0] = 0;
    }else{
      if (inChar > 0){
        for (uint16_t i = 0; i<MAX_INCOME_CMD_LENGTH; i++){
          if (income_serial[i] == 0){
            income_serial[i]   = inChar;
            income_serial[i+1] = 0;
            break;
          }
        }  
      }           
    }
  }  
}

// void plan_class::try_decode_and_run_cmd(){
//   // Тут мы будем пробовать рассшифровать комманду. 
//   // if (need_search_cmd_in_serial){
//   //   // Serial.print("try_decode_and_run_cmd");
//   //   // Serial.println(income_serial);
//   //   // clog("\n\try_decode_and_run_cmd=", income_serial);
//   //   process_command(strtol(&income_serial[0], NULL, 10)); 
//   //   income_serial[0] = 0;
//   //   need_search_cmd_in_serial = false;
//   // }
// }

void plan_class::pushIncomeCMD(uint16_t new_cmd){
  if (new_cmd > 0){
    // serial_echopair_P("Added new cmd=", new_cmd);
    uint8_t new_incomeBuffer_pos = incomeBuffer_pos +1;
    if (new_incomeBuffer_pos >= MAX_INCOME_CMD_STACK_SIZE) new_incomeBuffer_pos = 0;
    if (new_incomeBuffer_pos != incomeBuffer_ready){
      incomeBuffer[new_incomeBuffer_pos] = new_cmd+0;
      incomeBuffer_pos = new_incomeBuffer_pos+0;
    }else{
      #ifdef DEBUG
        SERIAL_ECHOPGM("process_lock_status=");
        Serial.print(process_lock_status);    
        SERIAL_ECHOPGM("incomeBuffer_pos=");
        Serial.print(incomeBuffer_pos);   
        SERIAL_ECHOPGM("incomeBuffer_ready=");
        Serial.println(incomeBuffer_ready);         
        SERIAL_ECHOLNPGM("HISTORY_INCOME_CMD_OVERFLOW");
      #endif
      pushHistoryCMD(HISTORY_INCOME_CMD_OVERFLOW);
    }
  }
}
void plan_class::pushHistoryCMD(uint8_t new_cmd){  
  for (uint8_t i = 0; i<HISTORY_BUFFER_SIZE; i++){
    if(s.history_timestamp[i]==0){
      s.history_timestamp[i] = secs;
      s.history_buffer[i] = new_cmd;
      return;
    }
  }
}





void plan_class::process_command() {
  // tmp222++;
  // if (tmp222 == 1000) {
  //   SERIAL_ECHOPGM("try process_command process_lock_status=");
  //   Serial.print(process_lock_status);
  //   SERIAL_ECHOPGM(" incomeBuffer_pos=");
  //   Serial.print(incomeBuffer_pos);
  //   SERIAL_ECHOPGM(" incomeBuffer_pos=");
  //   Serial.print(incomeBuffer_ready);
  //   Serial.println();
  //   tmp222=0;
  // }
  if (process_lock_status >= 2)  return;
       

  // clog("\n\nprocess_command=", "  ");
   // uint8_t a[4]={1,2,3,4};
   // cmd = *((uint32_t*) a);
  // uint8_t cmd_num = 1;
  // SERIAL_ECHOPGM("try run incomeBuffer_ready=");
  // Serial.print(incomeBuffer_ready);
  // SERIAL_ECHOPGM("try run incomeBuffer_pos=");
  // Serial.println(incomeBuffer_pos);
  // uin32_t i32 = v4[0] | (v4[1] << 8) | (v4[2] << 16) | (v4[3] << 24);
  // for (int i=0; i<4 ;++i) d[i] = ((uint8_t*)&c)[3-i];
  if (incomeBuffer_pos != incomeBuffer_ready){
    uint8_t new_incomeBuffer_ready = incomeBuffer_ready+1;    
    if (new_incomeBuffer_ready >= MAX_INCOME_CMD_STACK_SIZE) new_incomeBuffer_ready = 0;
    incomeBuffer_ready = new_incomeBuffer_ready;
    // uint16_t tmp = incomeBuffer[incomeBuffer_ready];
    // SERIAL_ECHOPGM("try run tmp=");
    // Serial.println(tmp);
    

    // uint32_t hc = htonl(cmd);
    uint8_t c[2] = {0,0}; // Раскладываем комманду из uint32_t в 4 составляющие uint8_t. 
    // for (uint8_t i=0; i<4;++i) c[i] = ((uint8_t*)&incomeBuffer[incomeBuffer_ready])[3-i];
    // for (uint8_t i=0; i<4;i++) c[i] = ((uint8_t*)&cmd)[i] + 0;   
    // for (uint8_t i=1; i>=0;i--) c[i] = ((uint8_t*)&tmp)[i] + 0;    
    for (uint8_t i=0; i<2;i++) c[1-i] = ((uint8_t*)&incomeBuffer[incomeBuffer_ready])[i] + 0;    
    #ifdef DEBUG
      Serial.println();
      for (uint8_t i=0; i<2;i++) clog(" ",c[i],false);  Serial.println();
    #endif 
    if (c[0] > 0) cmd_stack_pos++;
    uint8_t cmd_stack = cmd_stack_pos;
    switch(c[0]){
      case 0: // have no any cmd.
        return;
        break;
      case 1: // G1 
        SERIAL_ECHOLNPGM("G1 RUN");   
        break;
      case 2: // G2 reconnect_gsm
        if (1){
          process_lock_status = cmd_lock_thread;
          // SERIAL_ECHOLNPGM("G2 RUN [GPRS]");                
          while (cmd_stack == cmd_stack_pos) {
            // SERIAL_ECHOLNPGM("Resetting...");
            while ((!gprs->init()) && (cmd_stack == cmd_stack_pos)) {
              delay(3000);
            }
            // SERIAL_ECHOLNPGM("OK Resetting");
            
            // SERIAL_ECHOLNPGM("Setting up network...");
            uint8_t ret = gprs->setup();
            if (ret == 0) break; 
             
            #ifdef DEBUG
              SERIAL_ECHOPGM("Error code:");
              Serial.println(ret);
              Serial.println(gprs->buffer);
            #endif  
          }
          uint8_t a[2]={5,5};
          uint16_t cmd = *((uint16_t*) a);
          pushIncomeCMD(cmd); 
          if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
          return;
          // SERIAL_ECHOLNPGM("DONE Setting up network");
          if (cmd_stack == cmd_stack_pos){
            #ifdef DEBUG
              SERIAL_ECHOLNPGM("Setting up network OK");
              delay(3000);  
              if (gprs->getOperatorName()) {
                SERIAL_ECHOPGM("Operator:");
                Serial.println(gprs->buffer);
              }
              SERIAL_ECHOLNPGM("Try get qulity:");
              int ret = gprs->getSignalQuality();
              if (ret) {
                 SERIAL_ECHOPGM("Signal:");
                 Serial.print(ret);
                 SERIAL_ECHOLNPGM("dB");
              }            
              SERIAL_ECHOLNPGM("Try add cmd for http");
            #endif 
            uint8_t a[2]={3,3};
            uint16_t cmd = *((uint16_t*) a);
            pushIncomeCMD(cmd);
            if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
          }
        }       
        break;
      case 3: // try init http (it in stand alone for timeout 65 seconds)
        if (1){
          process_lock_status = cmd_lock_thread;          
          #ifdef DEBUG
            SERIAL_ECHOLNPGM("G3 [launch httpInit]"); 
          #endif
          for (uint8_t i = 0; ((i < 3) && (cmd_stack == cmd_stack_pos)) ; i++){
          // while ((!gprs->httpInit()) && (cmd_stack == cmd_stack_pos)) {
            if (gprs->httpInit()){
              uint8_t a[2]={5,5};
              uint16_t cmd = *((uint16_t*) a);
              pushIncomeCMD(cmd);  
              if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
              return;
            }
            // SERIAL_ECHOPGM("buffer_httpInit=["); 
            // Serial.print(gprs->buffer);
            // SERIAL_ECHOLNPGM("]"); 
            gprs->httpUninit();
            delay(300);
          }
          uint8_t a[2]={2,2};
          uint16_t cmd = *((uint16_t*) a);
          pushIncomeCMD(cmd);          
          if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
          return; 
        }
        break;
      case 4: // G4 Send data to server
        if (1){
          process_lock_status = cmd_lock_thread;    
          // #ifdef DEBUG0
          //   SERIAL_ECHOLNPGM("Requesting temp");           
          // #endif   
          // s1->_si_exists = false;
          // s2->_si_exists = false;      
          // if (!(s1->_si_exists)) s1->begin();   
          // if (!(s2->_si_exists)) s2->begin();   
          // delay(1000);     
          // s2->begin();                
          // delay(1000);  
          
          #ifdef DEBUG_REQUEST_URL
            SERIAL_ECHOPGM("Requesting ");
            SERIAL_ECHOPGM(request_url);
            // SERIAL_ECHOPGM("?");
            Serial.println(mydata);
          #endif          
          gprs->httpConnect(PSTR(request_url), mydata);
          // count++;
          while (gprs->httpIsConnected() == 0) {
            // can do something here while waiting
            // Serial.write('.');
            for (uint8_t n = 0; n < 25 && !gprs->available(); n++) {
              delay(10);
            }
          }
          if (gprs->httpState == HTTP_ERROR) {
            #ifdef DEBUG_REQUEST_URL
              SERIAL_ECHOLNPGM("Connect error");
            #endif
            http_errors++;
            gprs->httpUninit();
            delay(1500);
            uint8_t a[2]={3,3};
            uint16_t cmd = *((uint16_t*) a);
            pushIncomeCMD(cmd);   
            if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
            return; 
          }
          #ifdef DEBUG
            Serial.println();
          #endif
          gprs->httpRead();
          int ret = 0;
          while ((ret = gprs->httpIsRead()) == 0) {
            // can do something here while waiting
          }
          // SERIAL_ECHOLNPGM("httpIsRead done"); 
          if (gprs->httpState == HTTP_ERROR) {
            #ifdef DEBUG_REQUEST_URL
              SERIAL_ECHOLNPGM("Read error");
            #endif
            http_errors++;
            gprs->httpUninit();
            delay(1500);
            uint8_t a[2]={3,3};
            uint16_t cmd = *((uint16_t*) a);
            pushIncomeCMD(cmd);              
            if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
            return; 
          }          
          // now we have received payload

          
          // SERIAL_ECHOPGM("[Payload]");
          // Serial.println(gprs->buffer);
                  
          uint16_t cmd11= atoi(gprs->buffer);
          #ifdef DEBUG_REQUEST_URL
            SERIAL_ECHOPGM("income http cmd11=");  
            Serial.println(cmd11);
          #endif
          if (cmd11 > 0) without_send_data_seconds = 0;           
          if (cmd11 > 2560) {
            pushIncomeCMD(cmd11+0); 
          }
          // SERIAL_ECHOPGM("return now!"); return;
          // gprs->httpUninit();
          uint8_t a5[2]={5,5};
          // uint8_t a5[4]={3,3,3,3};
          uint16_t cmd223 = *((uint16_t*) a5);
          pushIncomeCMD(cmd223);   
          
          // SERIAL_ECHOPGM("cmd_stack=");
          // Serial.print(cmd_stack);
          // SERIAL_ECHOPGM(" cmd_stack_pos=");
          // Serial.println(cmd_stack_pos);
          // SERIAL_ECHOLNPGM("asdasd");
          // ClearToSend();
          // delay(500);
          if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
          // SERIAL_ECHOLNPGM("return now3!");
          return;       
        }
        break;
      case 5: 
        if (1){    
          process_lock_status = cmd_lock_thread;    
          #ifdef DEBUG0
            SERIAL_ECHOPGM("Requesting temp s1 broken=");           
            Serial.println(s1->broken);
          #endif         
          int16_t data_ints[4] = {0,0,0,0};
          if (!(s1->broken)){
            si7021_thc data = s1->getTempAndRH();
            #ifdef DEBUG0              
              SERIAL_ECHOPGM("data celsiusHundredths=");
              Serial.print(data.celsiusHundredths);
              SERIAL_ECHOPGM(" humidityPercent=");
              Serial.println(data.humidityPercent);    
            #endif              
            // sprintf(mydata, "h1=%d\&c1=%d", data.humidityPercent, data.celsiusHundredths);
            if (s1->broken) return;
            data_ints[0] = data.humidityPercent;
            data_ints[1] = data.celsiusHundredths;
          }
          if (!(s2->broken)){
            si7021_thc data2 = s2->getTempAndRH();
            #ifdef DEBUG0              
              SERIAL_ECHOPGM("data2 celsiusHundredths=");
              Serial.print(data2.celsiusHundredths);
              SERIAL_ECHOPGM(" humidityPercent=");
              Serial.println(data2.humidityPercent);    
            #endif  
            if (s2->broken) return;
            data_ints[2] = data2.humidityPercent;
            data_ints[3] = data2.celsiusHundredths;
          }
          sprintf(mydata, "h1=%d\&c1=%d\&h2=%d\&c2=%d", data_ints[0],data_ints[1],data_ints[2],data_ints[3]);
          delay(1000);
          uint8_t a5[2]={4,4};          
          // uint8_t a5[4]={5,5,5,5};
          uint16_t cmd223 = *((uint16_t*) a5);
          pushIncomeCMD(cmd223);  
          if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;
        }
        break;
      case 10: // G10 run to dest in percent
        if (1){    
          process_lock_status = cmd_lock_thread;  
          pinMode(POWER_SECONDARY_PIN, OUTPUT);
          digitalWrite(POWER_SECONDARY_PIN, HIGH);
          power_off_secondary_seconds = c[1];
          if (cmd_stack == cmd_stack_pos) process_lock_status = cmd_success;            
        }
        break;
      case 11: // G11 set global_motor_max_pwm_power
        break;
      case 12: // G12 read_travel_pin[T], read_max_speed_pin[S]         
        break;
      default:     
        #ifdef DEBUG   
          SERIAL_ECHO_START;
          SERIAL_ECHOPGM(MSG_UNKNOWN_COMMAND);
          // SERIAL_ECHO(cmdbuffer[bufindr]);
          SERIAL_ECHOLNPGM("\"");
        #endif
        break;      
    }
  // }else {
  //   SERIAL_ECHO_START;
  //   SERIAL_ECHOPGM(MSG_UNKNOWN_COMMAND);
  //   // SERIAL_ECHO(cmdbuffer[bufindr]);
  //   SERIAL_ECHOLNPGM("\"");
  }else{
    // process_lock_status = 1;
  }
  return;
  // ClearToSend();
}





