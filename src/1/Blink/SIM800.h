#ifndef SIM800_h
#define SIM800_h
/*************************************************************************
* SIM800 GPRS/HTTP Library
* Distributed under GPL v2.0
* Written by Stanley Huang <stanleyhuangyc@gmail.com>
* For more information, please visit http://arduinodev.com
*************************************************************************/

#include <Arduino.h>
// #include <String.h>
// #include <SoftwareSerial.h>
#include "SoftwareSerial.h"

// #define request_url "http://arduinodev.com/datetime.php"
#define request_url "http://chaky.ecomed.asdcode.com/htmls/dehumidifier.php\?do=add\&d=1\&key=123312123112\&"
// #define request_url "http://callmax.com.ua/gsmmon/dehumidifier.php\?do=add\&d=1\&key=123312123112\&"

// #include "language.h"
// #ifndef SIM_SERIAL3
//     #define SIM_SERIAL3
// static SoftwareSerial SIM_SERIAL2(12, 8);
static SoftwareSerial SIM_SERIAL2(13, 12);
// #define SIM_SERIAL3 SIM_SERIAL2

    static void serialprintPGM(const char *str) {
      char ch=pgm_read_byte(str);
      while(ch) {
        Serial.write(ch);
        ch=pgm_read_byte(++str);
      }
    }

    static void gsmPrintPGM(const char *str) {
      char ch=pgm_read_byte(str);
      while(ch) {
        SIM_SERIAL2.write(ch);
        ch=pgm_read_byte(++str);
      }
    }

    static String urlEncode(String str){
        String new_str = "";
        char c;
        int ic;
        const char* chars = str.c_str();
        char bufHex[10];
        int len = strlen(chars);

        for(int i=0;i<len;i++){
            c = chars[i];
            ic = c;
            // uncomment this if you want to encode spaces with +
            /*if (c==' ') new_str += '+';   
            else */if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') new_str += c;
            else {
                sprintf(bufHex,"%X",c);
                if(ic < 16) 
                    new_str += "%0"; 
                else
                    new_str += "%";
                new_str += bufHex;
            }
        }
        return new_str;
     }

    #define SERIAL_PROTOCOL(x) (Serial.print(x))
    #define SERIAL_PROTOCOL_F(x,y) (Serial.print(x,y))
    #define SERIAL_PROTOCOLPGM(x) (serialprintPGM(PSTR(x)))
    #define SERIAL_PROTOCOLLN(x) (Serial.print(x),Serial.write('\n'))
    #define SERIAL_PROTOCOLLNPGM(x) (serialprintPGM(PSTR(x)),Serial.write('\n'))
    #define GSM_PROTOCOLPGM(x) (gsmPrintPGM(PSTR(x)))
    #define GSM_PROTOCOLLNPGM(x) (gsmPrintPGM(PSTR(x)),SIM_SERIAL2.write('\n'))
    // #define GSM_PROTOCOLPGM(x) (serialprintPGM(PSTR(x)))
    // #define GSM_PROTOCOLLNPGM(x) (serialprintPGM(PSTR(x)))

    const char errormagic[] PROGMEM ="Error:";
    const char echomagic[] PROGMEM ="echo:";

    #define SERIAL_ERROR_START (serialprintPGM(errormagic))
    #define SERIAL_ERROR(x) SERIAL_PROTOCOL(x)
    #define SERIAL_ERRORPGM(x) SERIAL_PROTOCOLPGM(x)
    #define SERIAL_ERRORLN(x) SERIAL_PROTOCOLLN(x)
    #define SERIAL_ERRORLNPGM(x) SERIAL_PROTOCOLLNPGM(x)

    #define SERIAL_ECHO_START (serialprintPGM(echomagic))
    #define SERIAL_ECHO(x) SERIAL_PROTOCOL(x)
    #define SERIAL_ECHOPGM(x) SERIAL_PROTOCOLPGM(x)
    #define SERIAL_ECHOLN(x) SERIAL_PROTOCOLLN(x)
    #define SERIAL_ECHOLNPGM(x) SERIAL_PROTOCOLLNPGM(x)

    static void serial_echopair_P(const char *s_P, float v, bool new_str=true)          { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, double v, bool new_str=true)         { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, int32_t v, bool new_str=true)        { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, int16_t v, bool new_str=true)        { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, int8_t v, bool new_str=true)         { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, uint16_t v, bool new_str=true)       { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, uint32_t v, bool new_str=true)       { serialprintPGM(s_P); SERIAL_ECHO(v); if (new_str) Serial.println();}
    static void serial_echopair_P(const char *s_P, bool v, bool new_str=true)           { serialprintPGM(s_P); if (v){ SERIAL_ECHO("1"); }else SERIAL_ECHO("0"); if (new_str) Serial.println();}
     

// #endif
// SoftwareSerial * SIM_SERIAL3 = new SoftwareSerial(12, 8);
// #define SIM_SERIAL2  SIM_SERIAL3
// extern SoftwareSerial SIM_SERIAL2;
// #define SIM_SERIAL2 SIM_SERIAL3
// change this to the pin connect with SIM800 reset pin
// #define SIM800_RESET_PIN 9
#define SIM800_RESET_PIN 11
// SoftwareSerial gprsSerial(12, 8);
// change this to the serial UART which SIM800 is attached to
// #define SIM_SERIAL2 gprsSerial
// #define DEBUG0 1
// #define DEBUG 1
    // #define DEBUG_REQUEST_URL 1
// #define DEBUG2 1 // verbose level 2
// #define DEBUG_SENSOORS 1 // verbose level for SENSOORS    

//to one serial UART to enable debug information output
//#define DEBUG Serial

static void printPGM(const char *str) {
  char ch=pgm_read_byte(str);
  while(ch) {
    SIM_SERIAL2.write(ch);
    ch=pgm_read_byte(++str);
  }
}


typedef enum {
    HTTP_DISABLED = 0,
    HTTP_READY,
    HTTP_CONNECTING,
    HTTP_READING,
    HTTP_ERROR,
} HTTP_STATES;

typedef struct {
  float lat;
  float lon;
  uint8_t year; /* year past 2000, e.g. 15 for 2015 */
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
} GSM_LOCATION;

class CGPRS_SIM800 {
public:
    CGPRS_SIM800():httpState(HTTP_DISABLED) {}
    // initialize the module
    bool init();
    bool setBoudRate();
    // setup network
    uint8_t setup();
    // get network operator name
    bool getOperatorName();
    // check for incoming SMS
    bool checkSMS();
    // get signal quality level (in dB)
    int getSignalQuality();
    // get GSM location and network time
    // bool getLocation(GSM_LOCATION* loc);
    // initialize HTTP connection
    bool httpInit();
    // terminate HTTP connection
    void httpUninit();
    // connect to HTTP server
    bool httpConnect(const char in_url[] PROGMEM, const char* args = 0);
    // check if HTTP connection is established
    // return 0 for in progress, 1 for success, 2 for error
    uint8_t httpIsConnected();
    // read data from HTTP connection
    void httpRead();
    // check if HTTP connection is established
    // return 0 for in progress, -1 for error, bytes of http payload on success
    int httpIsRead();
    // send AT command and check for expected response
    uint8_t sendCommand(const char* cmd, unsigned int timeout = 10000, const char* expected = 0);
    uint8_t sendCommandPGM(const char cmd[] PROGMEM, unsigned int timeout = 10000, const char* expected = 0);    
    // send AT command and check for two possible responses
    uint8_t sendCommand(const char* cmd, const char* expected1, const char* expected2, unsigned int timeout = 10000);
    // toggle low-power mode
    bool sleep(bool enabled)
    {
      return sendCommand(enabled ? "AT+CFUN=0" : "AT+CFUN=1");
    }
    // check if there is available serial data
    bool available()
    {
      return SIM_SERIAL2.available(); 
    }
    char buffer[256];
    uint8_t httpState;
private:
    uint8_t checkbuffer(const char* expected1, const char* expected2 = 0, unsigned int timeout = 2000);
    void purgeSerial();
    uint8_t m_bytesRecv = 0;
    uint32_t m_checkTimer = 0;
};

#endif
