#ifndef plan_h
#define plan_h

#include <Arduino.h>
#include "cnt.h"
#include "Configuration.h"
#include "language.h"
#include "SIM800.h"
#include "SI7021.h"
#include <avr/wdt.h>
    
    #define POWER_SECONDARY_PIN 7
    
    #define MAX_INCOME_CMD_STACK_SIZE 5 // size=4 Размер входного стэка для поступающих из GSM модуля(или из сериал порта при отладке) комманд.
    #define DATA_FROM_SENSOR_SIZE 10    // size=6 Размер буфера для хранения значений, которые считаны с датчика. 1 значение весит 2(sensor1_buffer+sensor2_buffer)+4(sensor1_timestamp+sensor2_timestamp) = 6 байт....
    #define HISTORY_BUFFER_SIZE 10    // size=3  Размер буфера истории событий. (сюда будет помещаться события пропадания питания, ошибки подключения к серверу, ребуты оборудования)
    #define MAX_INCOME_CMD_LENGTH 10 // Длина буфера для Serial и GSM входной строки.

    #define clog(name,value,new_str) (serial_echopair_P(PSTR(name),(value),(new_str)))

   
typedef struct {    
  uint32_t last_unix_timestamp = 0;//timestamp at zero point.
  uint8_t  sensor1_buffer[DATA_FROM_SENSOR_SIZE];
  uint16_t sensor1_timestamp[DATA_FROM_SENSOR_SIZE];
  uint8_t  sensor2_buffer[DATA_FROM_SENSOR_SIZE];
  uint16_t sensor2_timestamp[DATA_FROM_SENSOR_SIZE];
  uint8_t  history_buffer[HISTORY_BUFFER_SIZE];
  uint16_t history_timestamp[HISTORY_BUFFER_SIZE];

} SETTINGS_STRUCT;


// Initialize the motion plan
class plan_class{
  public:        
    cnt_class   * cnt   = new cnt_class();
    SETTINGS_STRUCT s;
    plan_class();
    // TIME section
    void main_thread_tick();
    volatile uint16_t secs = 1; // seconds pass from last_unix_timestamp+1.(we cannot use 0 - becouse 0 mean no timestamp yet)
    uint16_t fast_ticks = 0;
    // SoftwareSerial * gprsSerial = new SoftwareSerial(7, 8);
    CGPRS_SIM800  *gprs = new CGPRS_SIM800();
    SI7021        *s1   = new SI7021(2);
    SI7021        *s2   = new SI7021(4);
    
    volatile uint16_t incomeBuffer[MAX_INCOME_CMD_STACK_SIZE];// 0-ой и 1-й байт - комманда, 2-й байт - позиция в строке или длинном числе, 3-й байт - само новое значение.
    volatile uint8_t incomeBuffer_pos = 0;
    volatile uint8_t incomeBuffer_ready = 0, cmd_stack_pos=0; //  incomeBuffer_pos - текущее значение полученных комманд в стэк. incomeBuffer_ready - последняя выполненная комманда из входного буфера.
    volatile uint8_t process_lock_status = 1;
    uint8_t power_off_secondary_seconds = 0;
    // uint16_t tmp222 = 0;
    // END TIME section
    char income_serial[MAX_INCOME_CMD_LENGTH+1], income_gsm[MAX_INCOME_CMD_LENGTH+1]; 
    char mydata[35];
    volatile bool need_search_cmd_in_serial = false,need_search_cmd_in_gsm = false;
    volatile uint16_t calc1 = 0,calc2 = 0,calc_0 = 0, tim2 = 0, http_errors = 0;
    volatile void init();

    void get_command();
    // void try_decode_and_run_cmd();
    void pushIncomeCMD( uint16_t new_cmd);
    void pushHistoryCMD( uint8_t new_cmd);
    
    volatile uint8_t without_send_data_seconds = 0;
    void process_command();

    void store_to_eeprom();
    void load_from_eeprom();

    bool timer_div_is_buzy[3] = {false, false,false};

    // void cmd_loop2();
    // void enquecommand(const char *cmd); //put an ASCII command at the end of the current buffer.
    // void enquecommand_P(const char *cmd); //put an ASCII command at the end of the current buffer, read from flash
    void send_debug_info();   
    static const uint8_t cmd_success     = 1;
    static const uint8_t cmd_lock_thread = 2;

};






#endif

