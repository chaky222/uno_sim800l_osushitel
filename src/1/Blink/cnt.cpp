
#include "cnt.h"
#include <avr/interrupt.h>
#include <avr/io.h>




cnt_class::cnt_class(){
  // launch_timers();
}

void cnt_class::launch_timers(){
  SREG = SREG | B10000000;  
  //Установка делителей таймера TCNT1H
  TCCR1A = B00000000;// Set Timer/Counter 1 in normal mode where // it will count to 0xFFFF then repeat.
  TIMSK1 = TIMSK1 | B00000001; // Turn on Timer/Counter 1 overflow // interrupt (bit 0).  
  // TCCR1B = TCCR1B | B00000001; // Turn on the counter with no prescaler.
  TCCR1B = 0x04;          // 16MHz clock with prescaler means TCNT1 increments every 4uS
  TCNT1 = 0;
  
    //Установка делителей таймера
  TCCR2A = 0;
  TCCR2B |= 1<<CS22;
  TCCR2B &= ~((1<<CS21) | (0<<CS20));
  TCCR2B &= ~((1<<WGM21) | (1<<WGM20));
  //Timer2 Overflow Interrupt Enable  
  TIMSK2 |= 1<<TOIE2; 
  //reset timer
  TCNT2 = 0;



}


