jQuery(function($){
var fileDiv = document.getElementById("upload");
var fileInput = document.getElementById("upload-image");
console.log(fileInput);
fileInput.addEventListener("change",function(e){
  var files = this.files
  showThumbnail(files)
},false)

fileDiv.addEventListener("click",function(e){
  $(fileInput).show().focus().click().hide();
  e.preventDefault();
},false)

fileDiv.addEventListener("dragenter",function(e){
  e.stopPropagation();
  e.preventDefault();
},false);

fileDiv.addEventListener("dragover",function(e){
  e.stopPropagation();
  e.preventDefault();
},false);

fileDiv.addEventListener("drop",function(e){
  e.stopPropagation();
  e.preventDefault();

  var dt = e.dataTransfer;
  var files = dt.files;

  showThumbnail(files)
},false);


function showThumbnail(files){
  for(var i=0;i<files.length;i++){
    var file = files[i]
    var imageType = /image.*/
    if(!file.type.match(imageType)){
      console.log("Not an Image");
      var file_id = vmaxs = file.name.replace(/[\. \(\)]/g, "");
      // alert(file_id);
      var $old = $('#file_'+file_id);
      if ($old.length  === 0) {
        $('#loaded_files').append('<label class="col-xs-12 src_label" style="padding-right:0px;"><input type="radio" name="plot_src" value="file_'+file_id+'" class="col-xs-1"><span class="file_name">'+file.name+'</span><span class="glyphicon glyphicon-edit act-tooltip" style="float:right; cursor:pointer;" data-toggle="tooltip" data-placement="top" data-original-title="View data and save"></span><input type="hidden" id="file_'+file_id+'" file_name="'+file.name+'" class="hidden_data" value="" style="display:none;"></label>');           
        $('.act-tooltip').tooltip({html:true});
        $('input[type="radio"][name="plot_src"][value="file_'+file_id+'"]').change(function() {
          redraw_flot();
          update();
        });
        $( "span.glyphicon.glyphicon-edit").unbind( "click", span_save_edit_file_handler );
        $( "span.glyphicon.glyphicon-edit").bind( "click", span_save_edit_file_handler );
      }
      var reader = new FileReader();
      reader.onload = function (aTxt) {
          var text = reader.result;
          $('#file_'+file_id).val(text);
       };

      
      reader.readAsText(file);
      // alert(file.name);
      // continue;
    }else{
      var image = document.createElement("img");
      // image.classList.add("")
      var thumbnail = document.getElementById("thumbnail");
      image.file = file;
      thumbnail.appendChild(image)

      var reader = new FileReader()
      reader.onload = (function(aImg){
        return function(e){
          aImg.src = e.target.result;
        };
      }(image))
      var ret = reader.readAsDataURL(file);
      var canvas = document.createElement("canvas");
      ctx = canvas.getContext("2d");
      image.onload= function(){
        ctx.drawImage(image,100,100)
      }
    }
    
  }
}
          });
var span_save_edit_file_handler = function() {
  save_data_dialog(this);
};
var hide_add_order_mark_popup = function (){ $('.add_order_mark,.modal_dialog_custom, .dialog_main_all, .modalOverlay').css({'opacity' : '0', 'visibility' : 'hidden'});}

var dialog_clear_file =         function (){
  var data_id = $('#show_text_file_dialog .file_text').attr('file_id');
  $('#'+data_id).val(''); 
  $('#show_text_file_dialog .file_text').html('');   
}

var save_content_to_file_handler = function() {
  // console.log('save_content_to_file_handler');
  save_content_to_file($('#show_text_file_dialog').find('.file_text').val(),$('#show_text_file_dialog').find('.file_name').html(),this);
}




// Set up the control widget
var updateInterval = 5000;
var RedrawInterval = 2000;
var redraw_timer;
function redraw_flot(){
  clearTimeout(redraw_timer);
  var cur_src = $('input[type="radio"][name="plot_src"]:checked').val(); 
  if (!(cur_src == 'server_data')){
    var data_text = $('input#'+cur_src).val();  
    if ((data_text) && (data_text.length>0)){    
      var data = data_from_text(data_text); 
      plot.setData(data);
      plot.draw();   
    }
    redraw_timer = setTimeout(redraw_flot, RedrawInterval);
  }  
}

$(document).ready(function() {    
    $("#updateInterval").val(updateInterval).change(function () {
      // alert('updateInterval');
      var v = parseInt($(this).val());    
      if (v < 1) {
        v = 1;
      } else if (v > 20000) {
        v = 20000;
      }
      updateInterval = v;
      $(this).val("" + updateInterval);      
    });
    $("#RedrawInterval").val(RedrawInterval).change(function () {
      // alert('updateInterval');
      var v = parseInt($(this).val());    
      if (v < 1) {
        v = 1;
      } else if (v > 20000) {
        v = 20000;
      }
      RedrawInterval = v;
      $(this).val("" + RedrawInterval);      
    });
    $('.act-tooltip').tooltip({html:true});
    // $('input[type="radio"][name="plot_src"]').change(function() {  update();  });
    $( "span.glyphicon.glyphicon-edit").bind( "click", span_save_edit_file_handler );
    $('input[type="radio"][name="plot_src"]').change(function() { redraw_flot(); update();  });
    $('#try_ajax').click(function() {  try_ajax();  });
    $('#refresh_data').click(function() {  data_to_flot();  });
    $('#clear_log_btn').click(function() {  $('#log_data').val('');  });
    
    
    $('.modalOverlay').bind( "click", hide_add_order_mark_popup );    
});




function save_data_dialog(elem){
  // alert($(elem).html());
  var $row = ($(elem).hasClass('src_label'))? $(elem) : $(elem).closest('.src_label');
  var data_id = $row.find('input[type="radio"][name="plot_src"]').val();
  var new_data = $('#file_show_dialog_sample').html();
  // var file_name = $row.find('.file_name').html();
  var $hf = $('#'+data_id);
  var file_text = $hf.val();
  var file_name = $hf.attr('file_name');
  load_dialog('show_text_file_dialog', new_data, 400,400);
  $('#show_text_file_dialog .file_name').html(file_name);
  $('#show_text_file_dialog .file_text').html(file_text);
  $('#show_text_file_dialog .file_text').attr('file_id',data_id);
}

function data_from_text(data_text){
  var data = [], val_num = 2;
  data[0] = [];
  data[1] = [];
  if (!data_text){
    data_text = "";
  }
  if (data_text.length>0){
    data_text = data_text.replace(/[^\,\. \n\d]/g, "");
    // console.log('data_text='+data_text);
    var myregexp2 = new RegExp("[\n ]+"); 
    var arr = data_text.split(myregexp2);  
    for (var i =0;i<arr.length; i++){    
      var val_pair = arr[i].split(',');
      if (val_pair.length >3){
        // alert(arr[i]);
        for (var i1=0;i1<2;i1++){
          var value_x = parseFloat(val_pair[0 + i1*2]);
          var value_y = parseFloat(val_pair[1 + i1*2]);
          data[i1].push([value_x, value_y]);
          // console.log('VALUES= '+arr[i]+" value_x="+value_x+' value_y='+value_y);
        }        
               
      }    
    }   
  }
  return data;
}

function data_to_text(data){
  var res = [], text='';
  for (var i = (data[0].length > totalPoints) ? (data[0].length - totalPoints) : 0; i < data[0].length; i++){ 
    for (var i1=0;i1<2;i1++){
      text += (i1<1)? data[i1][i][0]+","+data[i1][i][1] : ","+data[i1][i][0]+","+data[i1][i][1]+' ';
    }
    // res.push(data[i]);
    // text += data[i][0]+","+data[i][1]+","+data2[i][0]+","+data2[i][1]+" ";
    // console.log('text='+text);
    // alert('text='+text);
  }
  return text;
}

var plot;
var  totalPoints = 300;
function data_to_flot(){
  redraw_flot(); 
  // alert('data_to_flot');
  // console.log('data_to_flot');
  // var cur_src = $('input[type="radio"][name="plot_src"]:checked').val();  
  // var data_text = $('input#'+cur_src).val();  
  // if ((data_text) && (data_text.length>0)){    
  //   var data = data_from_text(data_text); 
  //   plot.setData(data);
  //   plot.draw();   
  // }
}

function myJsonMethod(dat){
  // alert('myJsonMethod');
  // logwork(dat);
}



function push_to_data_new_values(data, value){
   if (!data) data = [];
    while (data.length >= totalPoints) {
      data = data.slice(1);
    }
    // Do a random walk
    // while (data.length < totalPoints) {
      // var prev = data.length > 0 ? data[data.length - 1][1] : 50,
        // y = prev + Math.random() * 10 - 5;
      y = value;
      if (y < 0) {
        y = 0;
      } else if (y > 100) {
        y = 100;
      }
      data.push([data.length, y]);
    // }
    var res = [];
    for (var i = 0; i < data.length; ++i) {
      res.push([i, data[i][1]])
    }
    return res;

}
var last_update = 0;
var timer1;
function update() {
  clearTimeout(timer1);
  var cur_src = $('input[type="radio"][name="plot_src"]:checked').val();  
  if (cur_src == 'server_data'){
    var url = 'http://chaky.ecomed.asdcode.com/htmls/dehumidifier.php?do=getSensorData&last_update='+last_update;
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
      try {
        var income_str = this.responseText;

        // var arr = income_str.split("myJsonMethod(");
        // if (arr.length > 0){
        //   var str1 = arr[arr.length-1];
        //   arr = str1.split(')');
        var str2=income_str;
        var res = JSON.parse(str2);
        if (res.result > 0){
          last_update = res.last_update;
          // console.log('res=');
          // console.log(res);
          var data_text = $('input#'+cur_src).val();  
          var data =data_from_text(data_text);
          for (var i = 0; i<res.items.length; i++){
            var r = res.items[i];
            data[0] = push_to_data_new_values(data[0], r.val1);
            data[1] = push_to_data_new_values(data[1], r.val2);
          }
          // data[0] = push_to_data_new_values(data[0], res.value1);
          // data[1] = push_to_data_new_values(data[1], res.value2);
          var new_val = data_to_text(data);
          $('input#'+cur_src).val(new_val); 
          var data_text = new_val;  
          if ((data_text) && (data_text.length>0)){    
            var data = data_from_text(data_text); 
            plot.setData(data);
            plot.draw();   
          }
        }else{
          logwork('error in JSON(income_str):'+income_str);
        }          
        // }         
      } catch(e) {
        // Invalid JSON response
        logwork('error in JSON(responseText):'+this.responseText);
      }
      clearTimeout(timer1);
      timer1 = setTimeout(update, updateInterval);
    }
    xhr.onerror = function(e) {
      // Something bad happened
      logwork('Send error...');
      clearTimeout(timer1);
      timer1 = setTimeout(update, updateInterval);
    }
    xhr.open(
      "GET",
      url, 
      true
    );
    xhr.send();

  }else{
    if (cur_src == 'random'){   
      var data_text = $('input#'+cur_src).val();     
      var data =data_from_text(data_text);
      data[0] = getRandomData(data[0]);
      data[1] = getRandomData(data[1]);
      var new_val = data_to_text(data);
      // console.log(new_val);
      $('input#'+cur_src).val(new_val);   
      // plot.setData(data);
      // plot.draw();     
    }else{
      if (cur_src == 'arduino_serial'){   
        if ($("span#open").attr('connected')>0){
          arduinoSendStr('A');
        }
      }else{
        // var data_text = $('input#'+cur_src).val();  
        // var data =data_from_text(data_text);     
        // plot.setData(data);
        // plot.draw(); 

      }      
    }
    clearTimeout(timer1);
    timer1 = setTimeout(update, updateInterval);
  } 
}
function getRandomData(data) {
  if (!data) data = [];
  while (data.length >= totalPoints) {
    data = data.slice(1);
  }
  // Do a random walk
  while (data.length < totalPoints) {
    var prev = data.length > 0 ? data[data.length - 1][1] : 50,
      y = prev + Math.random() * 10 - 5;
    if (y < 0) {
      y = 0;
    } else if (y > 100) {
      y = 100;
    }
    data.push([data.length, y]);
  }
  var res = [];
  for (var i = 0; i < data.length; ++i) {
    res.push([i, data[i][1]])
  }
  return res;
}


$(function() {

    // We use an inline data source in the example, usually data would
    // be fetched from a server


    var data  = getRandomData() ;
    var data2 = getRandomData() ;
    var data_full = [data, data2];
    var new_val = data_to_text(data_full);
    // console.log('new_val='+new_val);
    $('input#random').val(new_val); 

    // $.plot("#placeholder", [
    //     { data: oilprices, label: "Oil price ($)" },
    //     { data: exchangerates, label: "USD/EUR exchange rate", yaxis: 2 }
    //   ], {
    //     xaxes: [ { mode: "time" } ],
    //     yaxes: [ { min: 0 }, {
    //       // align if we are to the right
    //       alignTicksWithAxis: position == "right" ? 1 : null,
    //       position: position,
    //       tickFormatter: euroFormatter
    //     } ],
    //     legend: { position: "sw" }
    //   });


    plot = $.plot("#placeholder", [
        { data: data, label: "Влажность" },
        { data: data2, label: "Температура", yaxis: 2 }
      ], {
      series: {
        shadowSize: 0 // Drawing is faster without shadows
      },
      yaxes: [ { min: 0,max: 100},{ min: 0,max: 200}],
      xaxis: {
        show: true
      },
      zoom: {
        interactive: true
      },
      pan: {
        interactive: true
      }

    });


    // plot = $.plot("#placeholder", [data], {
    //   series: {
    //     shadowSize: 0 // Drawing is faster without shadows
    //   },
    //   yaxis: {
    //     min: 0,
    //     max: 100
    //   },
    //   xaxis: {
    //     show: true
    //   },
    //   zoom: {
    //     interactive: true
    //   },
    //   pan: {
    //     interactive: true
    //   }

    // });


  });





function load_dialog(dialog_id,new_do_add,width,height,success_callback,par1,script_url){
  var bC = ((document.compatMode || 1) && !0) ? (document.compatMode == "CSS1Compat") ? document.documentElement.clientWidth : document.body.clientWidth : (document.parentWindow || document.defaultView).innerWidth;
  var bD = ((document.compatMode || 1) && !0) ? (document.compatMode == "CSS1Compat") ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;  
  var $diag = $('#'+dialog_id+'.dialog_main_all');
  if ($diag.length === 0) {  // значит ещё нету, надо создать  
    // alert('need_create');  
    $('body').append('<div id="'+dialog_id+'" class="dialog_main_all"></div>'); 
    $diag = $('#'+dialog_id+'.dialog_main_all');
    $diag.html(new_do_add);

    // if (success_callback) success_callback(par1);
    // $diag.html("Тащу инфу с сервера!......");
    // $.ajax({
    //   type: "POST",
    //   url: script_url,
    //   data: new_do_add+'&dialog_id='+dialog_id,
    //   success: function(msg){
    //     $diag.html(msg);
    //     if (success_callback) success_callback(par1);
    //   }
    // });    
  }else{
    $diag.html(new_do_add);
    // if (success_callback) success_scallback(par1);
  }  
    $diag.find('.close_btn').unbind("click", hide_add_order_mark_popup );
    $diag.find('.close_btn').bind(  "click", hide_add_order_mark_popup );
    $diag.find('.clear_btn').unbind("click", dialog_clear_file );
    $diag.find('.clear_btn').bind(  "click", dialog_clear_file );
    $diag.find('.save_btn').unbind("click", save_content_to_file_handler );
    $diag.find('.save_btn').bind(  "click", save_content_to_file_handler );

  if (width>0)  { $diag.css({'width'  :width+ 'px',});  }
  if (height>0) { $diag.css({'height' :height+'px',});  } 
  $diag.css({'top' : ((bD)/2)+'px', 'left' : ((bC )/2)+'px'});
  $('.modalOverlay').css({'opacity' : '1', 'visibility' : 'visible'});
  $diag.css({'opacity' : '1', 'visibility' : 'visible'});
  return $diag;
}

var connectionId;
function open_serial_port(){
//   console.log('try connect1!');
//   var clicks = $("button#open").attr('port_opened');
//   console.log('try connect2!');
//    if (!(port_opened >0)) {
//       $("button#open").attr('port_opened','1');
//       var port = $('select#portList').val();
//       console.log('try connect!');
//       chrome.serial.connect(port, {bitrate: 9600}, function(info) {
//         connectionId = info.connectionId;
//         $("button#open").html("close");
//         console.log('Connection opened with id: ' + connectionId + ', Bitrate: ' + info.bitrate);
//       });
//     } else {
//       chrome.serial.disconnect(connectionId, function(result) {
//         $("button#open").html("Open Port");
//         console.log('Connection with id: ' + connectionId + ' closed');
//       });
//     }
// }
}



$(document).ready(function() {
  update();
  redraw_flot();
  // Serial init here!!!
  // chrome.serial.getDevices(function(devices) {
  //   for (var i = 0; i < devices.length; i++) {
  //     $('select#portList').append('<option value="' + devices[i].path + '">' + devices[i].path + '</option>');
  //   }
  // });
  // $('span#open').click(function() {
  //     var clicks = $(this).data('clicks');
  //     if (!clicks) {
  //         var port = $('select#portList').val();
  //         chrome.serial.connect(port, {bitrate: 9600}, function(info) {
  //             connectionId = info.connectionId;
  //             $("span#open").html("Close");
  //             $("span#open").attr('connected','1');
  //             console.log('Connection opened with id: ' + connectionId + ', Bitrate: ' + info.bitrate);
  //         });
  //     } else {
  //         chrome.serial.disconnect(connectionId, function(result) {
  //             $("span#open").html("Open");
  //             $("span#open").attr('connected','0');
  //             console.log('Connection with id: ' + connectionId + ' closed');
  //         });
  //     }

      // $(this).data("clicks", !clicks);

  // });
});

var arduinoSendStr = function(ardSend){
  chrome.serial.send(connectionId, sendConvertString(ardSend), onSend);
}

//convert "ardSend" string to arduino serial-friendly format
var sendConvertString = function(ardSend) {

  var buf      = new ArrayBuffer(ardSend.length);
  var bufView  = new Uint8Array(buf);

  for (var i   = 0; i < ardSend.length; i++) {
    bufView[i] = ardSend.charCodeAt(i);
  }
  return buf;

}

var onError = function (errorInfo) {
  console.error("Received error on serial connection: " + errorInfo.error);
};



//Empty function for testing
var onSend = function(){
}
var serial_resived_string = "";
var onReceive = function(receiveInfo) {
  //ID test
  if (receiveInfo.connectionId !== connectionId) return;
  
  //create array from received arduino data
  var Int8View  = new Int8Array(receiveInfo.data);
  var encodedString = String.fromCharCode.apply(null, Int8View);
  serial_resived_string+=encodedString;
  pos = serial_resived_string.indexOf(')');
  if (pos>0){    
    // console.log('serial_resived_string='+serial_resived_string);   
    var arr = serial_resived_string.split("myJsonMethod(");
    if (arr.length > 0){
      var str1 = arr[arr.length-1];
      // console.log("str1=["+str1+"]");
      arr = str1.split(')');
      var str2=arr[0];
      serial_resived_string=arr[1];
      // console.log("str2=["+str2+"]");
      // console.log("str_last=["+serial_resived_string+"]");
      var msg = JSON.parse(str2);
    // console.log('result='+msg.result);
      if (msg.result > 0){
        var cur_src = "arduino_serial";  
        var data_text = $('input#'+cur_src).val();  
        var data =data_from_text(data_text);
        data[0] = push_to_data_new_values(data[0], msg.value1);
        data[1] = push_to_data_new_values(data[1], msg.value2);
        var new_val = data_to_text(data);
        $('input#'+cur_src).val(new_val); 
        // plot.setData(data);
        // plot.draw(); 
      }
    } 

    // serial_resived_string = serial_resived_string.replace(/^[\s\S]*?myJsonMethod\(/g, "");
    // serial_resived_string = serial_resived_string.replace(/\)/g, "");
    // console.log('serial_resived_string='+serial_resived_string);
   




  }

  // console.log('encodedString='+encodedString+"\n");
  //divide encoded string data (pin/value), it also verify the data.
  // var Atest = encodedString.indexOf("A");
  // var Btest = encodedString.indexOf("E");
  // var Ctest = encodedString.indexOf("V");
  // var Dtest = encodedString.length;

  // /*
  //   Example block of encoded data (Pin A3 value 872):
  //   A3V872E
  // */

  // //corrupted serial data parameters (Based on my observations)
  // //remove corrupted serial data from array list
  // if (  Atest == 0 && 
  //       Btest >= 2 && 
  //       Ctest >= 1 && 
  //       Dtest >= 4    ) {
    
  //   //pin counter
  //   var i = parseInt(encodedString.substring(1,Ctest));

  //   var stringValue = encodedString.substring(Ctest+1,Btest);
  //   var stringValueCheck = isNaN(stringValue);

  //   //count each analog pin number and create array of their values
  //   if (stringValueCheck == false){
  //     analogPins[i] = parseInt(stringValue);  
  //   }
  //   else {
  //     analogPins[i] = stringValue; 
  //   }

  // }

};

function logwork(log){
  $('#log_data').val(log+"\n"+$('#log_data').val());
}
// chrome.serial.onReceive.addListener(onReceive);

// chrome.serial.onReceiveError.addListener(onError);

var MIME_TYPE = 'text/plain';
function save_content_to_file(content, filename, elem){
  // console.log('save_content_to_file');
  window.URL = window.webkitURL || window.URL;
  var bb = new Blob([content], {type: MIME_TYPE});
  var a = document.createElement('a');
  a.download = filename;
  a.href = window.URL.createObjectURL(bb);
  a.textContent = 'Download ready';
  a.dataset.downloadurl = [MIME_TYPE, a.download, a.href].join(':');
  a.draggable = true; // Don't really need, but good practice.
  a.classList.add('dragout');
  $(elem).parent().append(a);
  a.click();
  $(a).remove();
};


function try_ajax(){  

  // $.ajax({    
  //   url: 'http://vint.com.ua/cgi-bin/json.cgi',
  //   dataType: 'JSONP',
  //   // jsonp: false,
  //   jsonpCallback: 'myJsonMethod',
  //   type: 'GET',
  //   success: function (data) {
  //       alert(data.result);
  //   }
  // });


}

function try_ajax() {
  console.log('ajax');
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    console.log('resp='+this.responseText);
    try {

      var result = JSON.parse(this.responseText);
      // Do something with the result
    } catch(e) {
      // Invalid JSON response
      console.log('e');
    }
  }
  xhr.onerror = function(e) {
    // Something bad happened
    console.log('onerror');
  }
  xhr.open(
    "GET",
    "http://192.168.0.177?E1", 
    true
  );
  xhr.send();
}
