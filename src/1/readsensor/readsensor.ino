#include <Wire.h>
#include "SI7021.h"
    

SI7021 sensor, sensor2;
int led1 = 3;
int led2 = 4;

void setup() {
    Serial.begin(115200);
    Serial.println("L");
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    sensor2.soft_i2c = true;
    // sensor.begin(A5, A4);
    Serial.println("try wire begin");
    sensor.begin();
    sensor2.begin();
    Serial.println("wire runned");
}


void loop() {
    // sensor2.soft_i2c = false;
    // // temperature is an integer in hundredths
    // Serial.println("try get temperature");
    // int temperature = sensor.getCelsiusHundredths();
    // temperature = temperature / 100;
    // Serial.print("temperature=");
    // Serial.println(temperature);
    
    // delay(1000);
    
    // // humidity is an integer representing percent
    // int humidity = sensor.getHumidityPercent();
    // Serial.print("humidity=");
    // Serial.println(humidity);
    // // for (int i = 0; i < humidity; i++) {
    // //     pulse(led2); 
    // // }
    
    // delay(1000);
    
    // // this driver should work for SI7020 and SI7021, this returns 20 or 21
    // int deviceid = sensor.getDeviceId();
    // Serial.print("deviceid=");
    // Serial.println(deviceid);
    // delay(1000);

    // enable internal heater for testing
    // sensor.setHeater(true);
    // delay(20000);
    // sensor.setHeater(false);
    
    // // see if heater changed temperature
    // temperature = sensor.getCelsiusHundredths();
    // temperature = temperature / 100;
    // for (int i = 0; i < temperature; i++) {
    //     pulse(led2); 
    // }
    
    // //cool down
    // delay(20000);

    // get humidity and temperature in one shot, saves power because sensor takes temperature when doing humidity anyway
    if (sensor._si_exists){
      si7021_thc data = sensor.getTempAndRH();
      Serial.print("data1 celsiusHundredths=");
      Serial.print(data.celsiusHundredths);
      Serial.print(" humidityBasisPoints=");
      Serial.println(data.humidityPercent);
    }else Serial.println("Sensor 1 is absent");
    

    if (sensor2._si_exists){
        si7021_thc data2 = sensor2.getTempAndRH();
        Serial.print("data2 celsiusHundredths=");
        Serial.print(data2.celsiusHundredths);
        Serial.print(" humidityBasisPoints=");
        Serial.println(data2.humidityPercent);
    }else Serial.println("Sensor 2 is absent");
    
    // for (int i = 0; i < data.celsiusHundredths/100; i++) {
    //     pulse(led1); 
    // }
    // for (int i = 0; i < data.humidityBasisPoints/100; i++) {
    //     pulse(led2); 
    // }
    delay(3000);
}

void pulse(int pin) {
   // software pwm, flash light for ~1 second with short duty cycle
   for (int i = 0; i < 20; i++) {
       digitalWrite(pin, HIGH);
       delay(1);
       digitalWrite(pin,LOW);
       delay(9);
   }
   digitalWrite(pin,LOW);
   delay(300);
}