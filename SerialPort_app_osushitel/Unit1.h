//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include "sPanel.hpp"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "sTrackBar.hpp"
#include "CSPIN.h"

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TComboBox *ComboBox1;
        TLabel *Label2;
        TComboBox *ComboBox2;
        TGroupBox *GroupBox2;
        TMemo *Memo1;
        TSpeedButton *SpeedButton1;
        TStatusBar *StatusBar1;
        TCheckBox *CheckBox1;
        TCheckBox *CheckBox2;
        TButton *Button3;
        TEdit *Edit1;
        TButton *Button1;
        TsPanel *sPanel1;
        TsPanel *sPanel2;
        TsPanel *sPanel3;
  TTimer *Timer1;
        TPanel *Panel1;
        TCSpinEdit *CSpinEdit1;
        TCSpinEdit *CSpinEdit2;
        TCSpinEdit *CSpinEdit3;
        TCSpinEdit *CSpinEdit4;
        TButton *Button2;
        void __fastcall SpeedButton1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall CheckBox2Click(TObject *Sender);
        void __fastcall DBForm_buttonClick(TObject *Sender);
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall DBGrid1DblClick(TObject *Sender);
  void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
        void __fastcall run_cmd(String s, bool print_error = true);
        void __fastcall received_cmd(String s, bool print_error = true);
        void __fastcall do_cmd(String cmd,String p1,String p2,String p3,String p4,String p5);

        bool DBcreated;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
