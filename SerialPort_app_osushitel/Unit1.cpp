//=============================================================================
//................................. � ��������� ...............................
//.............................................................................
//....... ��������� ������������� ��� ������������ ������ � COM-������ ........
//....... � ������� ������� � �������������� ����������� �������� .............
//.............................................................................
//.. ������������ ������ WINAPI � ��������� ResumeThread() � SuspendThread() ..
//.............................................................................
//=============================================================================

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <io.h>         //��� ������ � �������
#include <fcntl.h>      //��� ������ � �������
#include <sys\stat.h>   //��� ������ � �������

#include "Unit1.h"
#include "Unit2.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "sPanel"
#pragma link "ZConnection"
#pragma link "sTrackBar"
#pragma link "CSPIN"
#pragma resource "*.dfm"
TForm1 *Form1;
//TDBForm *DBForm1;

USEFORM("Unit2.cpp", DBForm);
//=============================================================================
//..................... ���������� ���������� ���������� ......................
//=============================================================================

#define BUFSIZE 255     //������� ������

unsigned char bufrd[BUFSIZE], bufwr[BUFSIZE]; //������� � ���������� ������
String print_str = "";

//---------------------------------------------------------------------------

HANDLE COMport;		//���������� �����

//��������� OVERLAPPED ���������� ��� ����������� ��������, ��� ���� ��� �������� ������ � ������ ����� �������� ������ ���������
//��� ��������� ���������� �������� ���������, ����� ��������� �� ����� �������� ���������
OVERLAPPED overlapped;		//����� ������������ ��� �������� ������ (��. ����� ReadThread)
OVERLAPPED overlappedwr;       	//����� ������������ ��� �������� ������ (��. ����� WriteThread)

//---------------------------------------------------------------------------

//int handle;             	//���������� ��� ������ � ������ � ������� ���������� <io.h>

//---------------------------------------------------------------------------

bool fl=0;	//����, ����������� �� ���������� �������� ������ (1 - �������, 0 - �� �������)

unsigned long counter;	//������� �������� ������, ���������� ��� ������ �������� �����


//=============================================================================
//.............................. ���������� ������� ...........................
//=============================================================================

void COMOpen(void);             //������� ����
void COMClose(void);            //������� ����


//=============================================================================
//.............................. ���������� ������� ...........................
//=============================================================================

HANDLE reader;	//���������� ������ ������ �� �����
HANDLE writer;	//���������� ������ ������ � ����

DWORD WINAPI ReadThread(LPVOID);
DWORD WINAPI WriteThread(LPVOID);


//=============================================================================
//.............................. ���������� ������� ...........................
//=============================================================================

//-----------------------------------------------------------------------------
//............................... ����� ReadThead .............................
//-----------------------------------------------------------------------------

void ReadPrinting(void);

//---------------------------------------------------------------------------

//������� ������� ������, ��������� ���� ������ �� COM-�����
DWORD WINAPI ReadThread(LPVOID)
{
 COMSTAT comstat;		//��������� �������� ��������� �����, � ������ ��������� ������������ ��� ����������� ���������� �������� � ���� ������
 DWORD btr, temp, mask, signal;	//���������� temp ������������ � �������� ��������

 overlapped.hEvent = CreateEvent(NULL, true, true, NULL);	//������� ���������� ������-������� ��� ����������� ��������
 SetCommMask(COMport, EV_RXCHAR);                   	        //���������� ����� �� ������������ �� ������� ����� ����� � ����
 while(1)						//���� ����� �� ����� �������, ��������� ����
  {
   WaitCommEvent(COMport, &mask, &overlapped);               	//������� ������� ����� ����� (��� � ���� ������������� ��������)
   signal = WaitForSingleObject(overlapped.hEvent, INFINITE);	//������������� ����� �� ������� �����
   if(signal == WAIT_OBJECT_0)				        //���� ������� ������� ����� ���������
    {
     if(GetOverlappedResult(COMport, &overlapped, &temp, true)) //���������, ������� �� ����������� ������������� �������� WaitCommEvent
      if((mask & EV_RXCHAR)!=0)					//���� ��������� ������ ������� ������� �����
       {
        ClearCommError(COMport, &temp, &comstat);		//����� ��������� ��������� COMSTAT
        btr = comstat.cbInQue;                          	//� �������� �� �� ���������� �������� ������
        if(btr)                         			//���� ������������� ���� ����� ��� ������
        {
         ReadFile(COMport, bufrd, btr, &temp, &overlapped);     //��������� ����� �� ����� � ����� ���������
         counter+=btr;                                          //����������� ������� ������
         ReadPrinting();                      		//�������� ������� ��� ������ ������ �� ����� � � ����
        }
       }
    }
  }
}

//---------------------------------------------------------------------------

//������� �������� ����� �� ����� � � ���� (���� ��������)
void ReadPrinting()
{
 //Form1->Memo1->Lines->Add((char*)bufrd);	 //������� �������� ������ � Memo
 for (int i=0;i<BUFSIZE;i++){
    byte c = bufrd[i];
    if (c==0){
        break;
    }
    //Form1->Memo1->Lines->Add(IntToStr(c));
    if ((c==10)||(c==13)){
       // print_str = print_str+(AnsiString)(char)13+(AnsiString)(char)10;
       if (print_str.Length() >0){
         Form1->received_cmd(print_str);
       }            
        print_str="";
    }else{
        print_str = print_str+(char)c;
    }
 }

 Form1->StatusBar1->Panels->Items[2]->Text = "����� ������� " + IntToStr(counter) + " ����";	//������� ������� � ������ ���������

// if(Form1->CheckBox3->Checked == true)  //���� ������� ����� ������ � ����
 // {
 //  write(handle, bufrd, strlen(bufrd)); //�������� � ���� ������ �� �������� ������
 // }
 memset(bufrd, 0, BUFSIZE);   	        //�������� ����� (����� ������ �� ������������� ���� �� �����)
}

//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//............................... ����� WriteThead ............................
//-----------------------------------------------------------------------------

//---------------------------------------------------------------------------

//������� ������� ������, ��������� �������� ������ �� ������ � COM-����
DWORD WINAPI WriteThread(LPVOID)
{
 DWORD temp, signal;

 overlappedwr.hEvent = CreateEvent(NULL, true, true, NULL);   	  //������� �������
 while(1)
  {WriteFile(COMport, bufwr, strlen(bufwr), &temp, &overlappedwr);  //�������� ����� � ���� (������������� ��������!)
   signal = WaitForSingleObject(overlappedwr.hEvent, INFINITE);	  //������������� �����, ���� �� ���������� ������������� �������� WriteFile

   if((signal == WAIT_OBJECT_0) && (GetOverlappedResult(COMport, &overlappedwr, &temp, true)))	//���� �������� ����������� �������
     {
      Form1->StatusBar1->Panels->Items[0]->Text  = "Sended success";    //������� ��������� �� ���� � ������ ���������
     }
   else {Form1->StatusBar1->Panels->Items[0]->Text  = "Error while sending";} 	//����� ������� � ������ ��������� ��������� �� ������

   SuspendThread(writer);
   
  }
}

//---------------------------------------------------------------------------


//=============================================================================
//............................. �������� ����� ................................
//=============================================================================

//---------------------------------------------------------------------------

//����������� �����, ������ � ��� ����������� ������������� ��������� �����
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner)
{
 //������������� ��������� ����� ��� ������� ���������

 Form1->Button1->Enabled = false;
 Form1->CheckBox1->Enabled = false;
 Form1->CheckBox2->Enabled = false;
}

//---------------------------------------------------------------------------

//���������� ������� �� ������ "������� ����"
void __fastcall TForm1::SpeedButton1Click(TObject *Sender)
{
 if(SpeedButton1->Down)
  {
   COMOpen();                   //���� ������ ������ - ������� ����

   //��������/�������� �������� �� �����
   Form1->ComboBox1->Enabled = false;
   Form1->ComboBox2->Enabled = false;
   Form1->Button1->Enabled = true;
   Form1->CheckBox1->Enabled = true;
   Form1->CheckBox2->Enabled = true;

   Form1->SpeedButton1->Caption = "Close Port";	//������� ������� �� ������

   counter = 0;	//���������� ������� ������

   //���� ���� �������� ������ DTR � RTS, ���������� ��� ����� � �������
   Form1->CheckBox1Click(Sender);
   Form1->CheckBox2Click(Sender);
  }

 else
  {
   COMClose();                  //���� ������ ������ - ������� ����

   Form1->SpeedButton1->Caption = "Open serialPort";	//������� ������� �� ������
   Form1->StatusBar1->Panels->Items[0]->Text = "";	//�������� ������ ������� ������ ���������

   //��������/�������� �������� �� �����
   Form1->ComboBox1->Enabled = true;
   Form1->ComboBox2->Enabled = true;
   Form1->Button1->Enabled = false;
   Form1->CheckBox1->Enabled = false;
   Form1->CheckBox2->Enabled = false;
  }
}

//---------------------------------------------------------------------------

//���������� �������� �����
void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
 COMClose();
}
//---------------------------------------------------------------------------


//������ "��������"
void __fastcall TForm1::Button1Click(TObject *Sender)
{
    run_cmd(Form1->Edit1->Text);
}

//---------------------------------------------------------------------------

//������ "�������� ����"
void __fastcall TForm1::Button3Click(TObject *Sender)
{
 Form1->Memo1->Clear();	//�������� Memo1
}

//---------------------------------------------------------------------------

//������� "DTR"
void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{
 //���� ����������� - ���������� ����� DTR � �������, ����� - � ����
 if(Form1->CheckBox1->Checked) EscapeCommFunction(COMport, SETDTR);
 else EscapeCommFunction(COMport, CLRDTR);
}

//---------------------------------------------------------------------------

//������� "RTS"
void __fastcall TForm1::CheckBox2Click(TObject *Sender)
{
 //���� ����������� - ���������� ����� RTS � �������, ����� - � ����
 if(Form1->CheckBox2->Checked) EscapeCommFunction(COMport, SETRTS);
 else EscapeCommFunction(COMport, CLRRTS);
}

//---------------------------------------------------------------------------

//=============================================================================
//........................... ���������� ������� ..............................
//=============================================================================

//---------------------------------------------------------------------------

//������� �������� � ������������� �����
void COMOpen()
{
 String portname;     	 //��� ����� (��������, "COM1", "COM2" � �.�.)
 DCB dcb;                //��������� ��� ����� ������������� ����� DCB
 COMMTIMEOUTS timeouts;  //��������� ��� ��������� ���������
 
 portname = Form1->ComboBox1->Text;	//�������� ��� ���������� �����

 //������� ����, ��� ����������� �������� ����������� ����� ������� ���� FILE_FLAG_OVERLAPPED
 COMport = CreateFile(portname.c_str(),GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
 //�����:
 // - portname.c_str() - ��� ����� � �������� ����� �����, c_str() ����������� ������ ���� String � ������ � ���� ������� ���� char, ����� ������� �� ������
 // - GENERIC_READ | GENERIC_WRITE - ������ � ����� �� ������/�������
 // - 0 - ���� �� ����� ���� ������������� (shared)
 // - NULL - ���������� ����� �� �����������, ������������ ���������� ������������ �� ���������
 // - OPEN_EXISTING - ���� ������ ����������� ��� ��� ������������ ����
 // - FILE_FLAG_OVERLAPPED - ���� ���� ��������� �� ������������� ����������� ��������
 // - NULL - ��������� �� ���� ������� �� ������������ ��� ������ � �������

 if(COMport == INVALID_HANDLE_VALUE)            //���� ������ �������� �����
  {
   Form1->SpeedButton1->Down = false;           //������ ������
   Form1->StatusBar1->Panels->Items[0]->Text = "Could not open port";       //������� ��������� � ������ ���������
   return;
  }

 //������������� �����

 dcb.DCBlength = sizeof(DCB); 	//� ������ ���� ��������� DCB ���������� ������� � �����, ��� ����� �������������� ��������� ��������� ����� ��� �������� ������������ ���������

 //������� ��������� DCB �� �����
 if(!GetCommState(COMport, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
  {
   COMClose();
   Form1->StatusBar1->Panels->Items[0]->Text  = "�� ������� ������� DCB";
   return;
  }

 //������������� ��������� DCB
 dcb.BaudRate = StrToInt(Form1->ComboBox2->Text);       //����� �������� �������� 115200 ���
 dcb.fBinary = TRUE;                                    //�������� �������� ����� ������
 dcb.fOutxCtsFlow = FALSE;                              //��������� ����� �������� �� �������� CTS
 dcb.fOutxDsrFlow = FALSE;                              //��������� ����� �������� �� �������� DSR
 dcb.fDtrControl = DTR_CONTROL_DISABLE;                 //��������� ������������� ����� DTR
 dcb.fDsrSensitivity = FALSE;                           //��������� ��������������� �������� � ��������� ����� DSR
 dcb.fNull = FALSE;                                     //��������� ���� ������� ������
 dcb.fRtsControl = RTS_CONTROL_DISABLE;                 //��������� ������������� ����� RTS
 dcb.fAbortOnError = FALSE;                             //��������� ��������� ���� �������� ������/������ ��� ������
 dcb.ByteSize = 8;                                      //����� 8 ��� � �����
 dcb.Parity = 0;                                        //��������� �������� ��������
 dcb.StopBits = 0;                                      //����� ���� ����-���

 //��������� ��������� DCB � ����
 if(!SetCommState(COMport, &dcb))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
  {
   COMClose();
   Form1->StatusBar1->Panels->Items[0]->Text  = "Failed to set DCB";
   return;
  }

 //���������� ��������
 timeouts.ReadIntervalTimeout = 0;	 	//������� ����� ����� ���������
 timeouts.ReadTotalTimeoutMultiplier = 0;	//����� ������� �������� ������
 timeouts.ReadTotalTimeoutConstant = 0;         //��������� ��� ������ �������� �������� ������
 timeouts.WriteTotalTimeoutMultiplier = 0;      //����� ������� �������� ������
 timeouts.WriteTotalTimeoutConstant = 0;        //��������� ��� ������ �������� �������� ������

 //�������� ��������� ��������� � ����
 if(!SetCommTimeouts(COMport, &timeouts))	//���� �� ������� - ������� ���� � ������� ��������� �� ������ � ������ ���������
  {
   COMClose();
   Form1->StatusBar1->Panels->Items[0]->Text  = "�� ������� ���������� ����-����";
   return;
  }

 //���������� ������� �������� ����� � ��������
 SetupComm(COMport,2000,2000);
  
 //������� ��� ������� ������������ ���� ��� ������ ����������� ������



 PurgeComm(COMport, PURGE_RXCLEAR);	//�������� ����������� ����� �����

 reader = CreateThread(NULL, 0, ReadThread, NULL, 0, NULL);			//������ ����� ������, ������� ����� ������ ����������� (������������� �������� = 0)
 writer = CreateThread(NULL, 0, WriteThread, NULL, CREATE_SUSPENDED, NULL);	//������ ����� ������ � ������������� ��������� (������������� �������� = CREATE_SUSPENDED)

}

//---------------------------------------------------------------------------

//������� �������� �����
void COMClose()
{
//����������: ��� ��� ��� ���������� �������, ��������� � ������� ������� WinAPI, �������� TerminateThread
//	      ����� ����� ���� ������� �����, � ����� ����� ������ ����������, �� ����������� ����������
//	      ����������� �������-�������, ������������ � ��������� ���� OVERLAPPED, ��������� � �������,
//	      ������� �� ������ ���� ������, � ��������, ����� ������ ������� TerminateThread.
//	      ����� ���� ����� ���������� � ��� ���������� ������.

 if(writer)		//���� ����� ������ ��������, ��������� ���; �������� if(writer) �����������, ����� ��������� ������
  {TerminateThread(writer,0);
   CloseHandle(overlappedwr.hEvent);	//����� ������� ������-�������
   CloseHandle(writer);
  }
 if(reader)		   //���� ����� ������ ��������, ��������� ���; �������� if(reader) �����������, ����� ��������� ������
  {TerminateThread(reader,0);
   CloseHandle(overlapped.hEvent);	//����� ������� ������-�������
   CloseHandle(reader);
  }

 CloseHandle(COMport);                  //������� ����
 COMport=0;				//�������� ���������� ��� ����������� �����
}
void split(TStringList* lout, char* str, const char* separator) {       for(char* tok = strtok(str, separator); tok; tok = strtok(NULL, separator))          lout->Add(tok);}

void __fastcall TForm1::received_cmd(String s, bool print_error){  //
 // TStringList*  lst = new TStringList();
  String s2 = s+" ";
  //Form1->Memo1->Lines->Add("1["+s+"]");
 /* char *ch = new char[s.Length()];
  ch = s.c_str();
  split(lst, ch, " ");
  for(int i = lst->Count; i < 20; i++) {lst->Add("");}
  if (lst->Strings[0] == "[CMD]"){
    String s1 = "";
    int first_param = 1;
    if (lst->Strings[1] == "[HIDDEN]"){
      first_param=2;
    }else{
      first_param=1;
      for(int i = 1; i < lst->Count; i++) {s1 = s1+" "+lst->Strings[i];}
      Form1->Memo1->Lines->Add("["+s1+"]");
    }
    do_cmd(lst->Strings[first_param],lst->Strings[first_param+1],lst->Strings[first_param+2],lst->Strings[first_param+3],lst->Strings[first_param+4],lst->Strings[first_param+5]);
  }else{
    Form1->Memo1->Lines->Add("["+s2+"]");
  }    */
  Form1->Memo1->Lines->Add("["+s2+"]");
//  do_cmd();
  
}
void __fastcall TForm1::do_cmd(String cmd,String p1,String p2,String p3,String p4,String p5){//
  bool cmd_done = false;
  
}

//---------------------------------------------------------------------------
void __fastcall TForm1::run_cmd(String s, bool print_error){  //
  if (Button1->Enabled){
        //Memo1->Lines->Add("["+s+"]");
        String s1 =  s + "\n\n";
        memset(bufwr,0,BUFSIZE);			//�������� ����������� ���������� �����, ����� ������ �� ������������� ���� �� �����
        PurgeComm(COMport, PURGE_TXCLEAR);             //�������� ���������� ����� �����
        strcpy(bufwr,s1.c_str());      //������� � ����������� ���������� ����� ������ �� Edit1

        ResumeThread(writer);               //������������ ����� ������ ������ � ����
        Sleep(1);//wait for send
  }else{
    if (print_error){
      ShowMessage("Connect to the arduino board in first! Press Open Port button!");
    }
  }

}
//---------------------------------------------------------------------------
void __fastcall TForm1::DBForm_buttonClick(TObject *Sender)
{
//DBForm1 = new TDBForm(Form1);

  DBForm->Show();

}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormCreate(TObject *Sender)
{
  DBcreated = false;  
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormShow(TObject *Sender)
{
  if(!DBcreated){
     Application->CreateForm(__classid(TDBForm), &DBForm);
     DBcreated = true;  
  }  
}



void __fastcall TForm1::DBGrid1DblClick(TObject *Sender)
{
  Button1->Click();  
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
  run_cmd("G2",false);
}
//---------------------------------------------------------------------------















void __fastcall TForm1::Button2Click(TObject *Sender)
{
  int c1 = StrToInt(CSpinEdit1->Value),c2 = StrToInt(CSpinEdit2->Value),c3 = StrToInt(CSpinEdit3->Value),c4 = StrToInt(CSpinEdit4->Value);
  Cardinal ctmp4 = c4 << 8*3;
  Cardinal ctmp3 = c3 << 8*2;
  Cardinal ctmp = ctmp4+ctmp3+c2*256+c1;
  Edit1->Text = IntToStr(ctmp);
  Button1->Click();
}
//---------------------------------------------------------------------------

